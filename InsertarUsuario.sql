USE [Gestión_Matrícula]
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Usuario]    Script Date: 01/12/2021 22:59:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[Insertar_Usuario] 
  @Usuario varchar(60),
  @Contraseña varchar(60),
  @Rol varchar(60)
  as

  Insert Into Usuario(usuario, contraseña, rol, estado)
   VALUES
  (@Usuario, ENCRYPTBYPASSPHRASE(@contraseña, @Contraseña), @Rol,'Habilitado')