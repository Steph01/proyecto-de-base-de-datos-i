USE [Gestión_Matrícula]
GO
/****** Object:  StoredProcedure [dbo].[Validar_Acceso]    Script Date: 01/12/2021 22:59:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[Validar_Acceso]
@Usuario varchar(60),
@Contraseña varchar(60)
as
Declare @Resultado varchar(50)
if exists (Select top 1 * from Usuario
           where Usuario = @Usuario and DECRYPTBYPASSPHRASE(@Contraseña, contraseña) = @Contraseña
		   and Estado = 'Habilitado')
		   Begin

		set @Resultado = 'Acceso Exitoso'
		end
		else
		set @Resultado = 'Acceso Denegado'
Select @Resultado as Resultado