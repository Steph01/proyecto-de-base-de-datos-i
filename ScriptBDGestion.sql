USE [master]
GO
/****** Object:  Database [Gestión_Matrícula]    Script Date: 01/12/2021 19:23:13 ******/
CREATE DATABASE [Gestión_Matrícula]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Gestión_Matrícula', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Gestión_Matrícula.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Gestión_Matrícula_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Gestión_Matrícula_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Gestión_Matrícula] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Gestión_Matrícula].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Gestión_Matrícula] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET ARITHABORT OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Gestión_Matrícula] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Gestión_Matrícula] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Gestión_Matrícula] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Gestión_Matrícula] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET RECOVERY FULL 
GO
ALTER DATABASE [Gestión_Matrícula] SET  MULTI_USER 
GO
ALTER DATABASE [Gestión_Matrícula] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Gestión_Matrícula] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Gestión_Matrícula] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Gestión_Matrícula] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Gestión_Matrícula] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Gestión_Matrícula] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Gestión_Matrícula', N'ON'
GO
ALTER DATABASE [Gestión_Matrícula] SET QUERY_STORE = OFF
GO
USE [Gestión_Matrícula]
GO
/****** Object:  User [Roleb1]    Script Date: 01/12/2021 19:23:13 ******/
CREATE USER [Roleb1] FOR LOGIN [Roleb1] WITH DEFAULT_SCHEMA=[Roleb1]
GO
ALTER ROLE [db_owner] ADD MEMBER [Roleb1]
GO
/****** Object:  Schema [Roleb1]    Script Date: 01/12/2021 19:23:13 ******/
CREATE SCHEMA [Roleb1]
GO
/****** Object:  Table [dbo].[Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asignatura](
	[NoAsignatura] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[NoCreditos] [int] NULL,
	[Semestre] [varchar](2) NULL,
	[Clasificacion] [char](1) NULL,
	[NoDepartamento] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NoAsignatura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carrera]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carrera](
	[NoCarrera] [int] IDENTITY(1,1) NOT NULL,
	[IdFacultad] [int] NULL,
	[Nombre] [varchar](50) NULL,
	[Cantidad_Asignaturas] [int] NULL,
	[Créditos] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[NoCarrera] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carrera_Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carrera_Asignatura](
	[NoAsignatura] [int] NOT NULL,
	[NoCarrera] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departamento](
	[NoDepartamento] [int] IDENTITY(1,1) NOT NULL,
	[IdFacultad] [int] NOT NULL,
	[Nombre] [varchar](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NoDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Detalle_Matrícula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalle_Matrícula](
	[IdDetalleMatrícula] [int] IDENTITY(1,1) NOT NULL,
	[NoMatricula] [int] NOT NULL,
	[NoAsignatura] [int] NOT NULL,
	[Semestre] [int] NULL,
	[Aula] [int] NULL,
	[Grupo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDetalleMatrícula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estudiante](
	[IdEstudiante] [int] IDENTITY(1,1) NOT NULL,
	[NoCarrera] [int] NULL,
	[Primer Nombre] [varchar](50) NOT NULL,
	[Segundo Nombre] [varchar](50) NULL,
	[Primer Apellido] [varchar](50) NOT NULL,
	[Segundo Apellido] [varchar](50) NULL,
	[Fecha_Ingreso] [date] NOT NULL,
	[Dirección] [varchar](100) NOT NULL,
	[Teléfono] [int] NULL,
	[Correo] [varchar](60) NULL,
	[Edad] [int] NULL,
	[Estado] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEstudiante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Facultad]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facultad](
	[IdFacultad] [int] IDENTITY(1,1) NOT NULL,
	[Recinto] [varchar](50) NULL,
	[Nombre] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdFacultad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Matricula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Matricula](
	[NoMatrícula] [int] IDENTITY(1,1) NOT NULL,
	[IdEstudiante] [int] NULL,
	[Año] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NoMatrícula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profesor]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profesor](
	[Id Profesor] [int] IDENTITY(1,1) NOT NULL,
	[Primer Nombre] [varchar](50) NOT NULL,
	[Segundo Nombre] [varchar](50) NULL,
	[Primer Apellido] [varchar](50) NOT NULL,
	[Segundo Apellido] [varchar](50) NULL,
	[Dirección] [varchar](100) NOT NULL,
	[Teléfono] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id Profesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](60) NULL,
	[Contraseña] [varchar](60) NULL,
	[Rol] [varchar](60) NULL,
	[Estado] [varchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Asignatura] ON 

INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (1, N'Contabilidad Financiera', 3, N'1', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (2, N'Matemáticas I', 3, N'1', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (3, N'Introducción a la Programación', 3, N'1', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (4, N'Inglés I', 3, N'1', N'B', 5)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (6, N'Redacción Técnica', 3, N'1', N'F', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (7, N'Filosofía', 3, N'1', N'B', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (8, N'Contabilidad de Costo', 3, N'2', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (9, N'Algebra Líneal', 3, N'2', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (10, N'Matemática II', 3, N'2', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (11, N'Programación I', 3, N'2', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (12, N'Inglés II', 3, N'2', N'B', 5)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (13, N'Sociología', 3, N'2', N'F', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (14, N'Contabilidad Gerencial', 3, N'3', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (15, N'Ingeniería Económica', 3, N'3', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (16, N'Estadística I', 3, N'3', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (17, N'Matemática III', 3, N'3', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (18, N'Programación II', 3, N'3', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (19, N'Finanzas I', 3, N'4', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (20, N'Microeconomía', 3, N'4', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (21, N'Estadística II', 3, N'4', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (22, N'Base de datos I', 3, N'4', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (23, N'Física I', 3, N'4', N'B', 4)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (24, N'Mercadotecnia', 3, N'5', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (25, N'Macroeconomía', 3, N'5', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (26, N'Producción I', 3, N'5', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (27, N'Métodos Númericos', 3, N'5', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (28, N'Base de datos II', 3, N'5', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (29, N'Física II', 3, N'5', N'B', 4)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (30, N'Finanzas II', 3, N'6', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (31, N'Organización I', 3, N'6', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (32, N'Investigación de Operaciones I', 3, N'6', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (33, N'Producción II', 3, N'6', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (34, N'Ingeniería de Software I', 3, N'6', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (35, N'Arquitectura de Máquina', 3, N'6', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (36, N'Organización II', 3, N'7', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (37, N'Investigación de Operaciones II', 3, N'7', N'B', 1)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (38, N'Producción III', 3, N'7', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (39, N'Ingeniería de Software II', 3, N'7', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (40, N'Sistemas Operativos', 3, N'7', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (41, N'Aplicaciones Gráficas y Multimedia', 3, N'7', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (42, N'Metodología de la Investigación', 3, N'8', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (43, N'Ingeniería de Sistemas', 3, N'8', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (44, N'Inteligencia Artificial', 3, N'8', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (45, N'Sistemas Operativos de Redes', 3, N'8', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (46, N'Historia CA y Nicaragua', 3, N'8', N'F', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (47, N'Modelación y Simulación de Sistemas', 3, N'9', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (48, N'Sistemas de Información', 3, N'9', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (49, N'Auditoria de Sistemas', 3, N'9', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (50, N'Tecnología y Medio Ambiente', 3, N'9', N'F', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (51, N'Administración de Proyectos', 3, N'10', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (52, N'Sitemas de Manufactura', 3, N'10', N'B', 2)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (53, N'Administración Informática', 3, N'10', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (54, N'Diseño de Sistemas en Internet', 3, N'10', N'B', 3)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (55, N'Ética Profesional', 3, N'10', N'B', 6)
INSERT [dbo].[Asignatura] ([NoAsignatura], [Nombre], [NoCreditos], [Semestre], [Clasificacion], [NoDepartamento]) VALUES (56, N'Formulación y Evaluación de Proyectos', 3, N'9', N'B', 2)
SET IDENTITY_INSERT [dbo].[Asignatura] OFF
GO
SET IDENTITY_INSERT [dbo].[Carrera] ON 

INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (1, 1, N'Ingeniería de Sistemas', 50, N'3         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (2, 2, N'Ingeniería Civil', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (3, 2, N'Ingeniería Agrícola', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (4, 3, N'Ingeniería Mecánica', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (5, 3, N'Ingeniería Industrial', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (6, 5, N'Ingeniería en Computación', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (7, 5, N'Ingeniería en Computación', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (8, 5, N'Ingeniería en Telecomunicaciones', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (9, 5, N'Ingeniería Electrónica', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (10, 5, N'Ingeniería Eléctrica', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (11, 6, N'Ingeniería Química', 50, N'2         ')
INSERT [dbo].[Carrera] ([NoCarrera], [IdFacultad], [Nombre], [Cantidad_Asignaturas], [Créditos]) VALUES (13, 7, N'Ingeniería en Economía y Negocios', 50, N'2         ')
SET IDENTITY_INSERT [dbo].[Carrera] OFF
GO
SET IDENTITY_INSERT [dbo].[Departamento] ON 

INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (1, 1, N'Departamento de Matemáticas')
INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (2, 1, N'Departamento de Administración')
INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (3, 1, N'Departamento de Informática')
INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (4, 1, N'Departamento de Física')
INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (5, 1, N'Departamento de Idiomas')
INSERT [dbo].[Departamento] ([NoDepartamento], [IdFacultad], [Nombre]) VALUES (6, 1, N'Departamento de Áreas Básicas')
SET IDENTITY_INSERT [dbo].[Departamento] OFF
GO
SET IDENTITY_INSERT [dbo].[Detalle_Matrícula] ON 

INSERT [dbo].[Detalle_Matrícula] ([IdDetalleMatrícula], [NoMatricula], [NoAsignatura], [Semestre], [Aula], [Grupo]) VALUES (1, 1, 2, 1, 1031, N'1T1-IS')
SET IDENTITY_INSERT [dbo].[Detalle_Matrícula] OFF
GO
SET IDENTITY_INSERT [dbo].[Estudiante] ON 

INSERT [dbo].[Estudiante] ([IdEstudiante], [NoCarrera], [Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido], [Fecha_Ingreso], [Dirección], [Teléfono], [Correo], [Edad], [Estado]) VALUES (2, 10, N'Roy', N'Stephen', N'Omier', N'Guevara', CAST(N'2021-11-17' AS Date), N'roy.441@gmail.com', 76445412, N'SBN', 20, N'Activo')
INSERT [dbo].[Estudiante] ([IdEstudiante], [NoCarrera], [Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido], [Fecha_Ingreso], [Dirección], [Teléfono], [Correo], [Edad], [Estado]) VALUES (3, 4, N'Rodolfo', N'Alberto', N'Montoya', N'Canales', CAST(N'2021-11-17' AS Date), N'Avenida 572', 88965785, N'ktou@yahoo.com', 23, N'Activo')
SET IDENTITY_INSERT [dbo].[Estudiante] OFF
GO
SET IDENTITY_INSERT [dbo].[Facultad] ON 

INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (1, N'Recinto Universitario Pedro Arauz Palacios', N'Facultad de Ciencias y Sistemas')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (2, N'Recinto Universitario Pedro Arauz Palacios', N'Facultad de Tecnología de la Construcción')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (3, N'Recinto Universitario Pedro Arauz Palacios', N'Facultad de Tecnología de la Industria')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (4, N'Recinto Universitario Simón Bolivar', N'Facultad de Arquitectura')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (5, N'Recinto Universitario Simón Bolivar', N'Facultad de Electrotécnica y Computación')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (6, N'Recinto Universitario Simón Bolivar', N'Facultad de Ingeniería Química')
INSERT [dbo].[Facultad] ([IdFacultad], [Recinto], [Nombre]) VALUES (7, N'Recinto Universitario Simón Bolivar', N'Facultad de Economía y Negocios')
SET IDENTITY_INSERT [dbo].[Facultad] OFF
GO
SET IDENTITY_INSERT [dbo].[Matricula] ON 

INSERT [dbo].[Matricula] ([NoMatrícula], [IdEstudiante], [Año]) VALUES (1, 2, 2020)
INSERT [dbo].[Matricula] ([NoMatrícula], [IdEstudiante], [Año]) VALUES (2, 3, 2020)
SET IDENTITY_INSERT [dbo].[Matricula] OFF
GO
SET IDENTITY_INSERT [dbo].[Profesor] ON 

INSERT [dbo].[Profesor] ([Id Profesor], [Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido], [Dirección], [Teléfono]) VALUES (1, N'Roy', N'Stephen', N'Omier', N'Guevara', N'x', 76445412)
INSERT [dbo].[Profesor] ([Id Profesor], [Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido], [Dirección], [Teléfono]) VALUES (2, N'Rolando', N'Daniel', N'Peñalba', N'Martínez', N'454848', 58082303)
SET IDENTITY_INSERT [dbo].[Profesor] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([IdUsuario], [Usuario], [Contraseña], [Rol], [Estado]) VALUES (1, N'RRP', N'   ¥êšvÆBy
@Ù¶•
:!¥?»ÛòG´w¢*&‚˜N$LM÷¯3T‹…L“l', N'Administrador', N'Habilitado')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
ALTER TABLE [dbo].[Asignatura]  WITH CHECK ADD  CONSTRAINT [FK_Asignatura_Departamento] FOREIGN KEY([NoDepartamento])
REFERENCES [dbo].[Departamento] ([NoDepartamento])
GO
ALTER TABLE [dbo].[Asignatura] CHECK CONSTRAINT [FK_Asignatura_Departamento]
GO
ALTER TABLE [dbo].[Carrera]  WITH CHECK ADD  CONSTRAINT [FK_Carrera_Facultad] FOREIGN KEY([IdFacultad])
REFERENCES [dbo].[Facultad] ([IdFacultad])
GO
ALTER TABLE [dbo].[Carrera] CHECK CONSTRAINT [FK_Carrera_Facultad]
GO
ALTER TABLE [dbo].[Carrera_Asignatura]  WITH CHECK ADD  CONSTRAINT [FK_Carrera_Asignatura_Asignatura1] FOREIGN KEY([NoAsignatura])
REFERENCES [dbo].[Asignatura] ([NoAsignatura])
GO
ALTER TABLE [dbo].[Carrera_Asignatura] CHECK CONSTRAINT [FK_Carrera_Asignatura_Asignatura1]
GO
ALTER TABLE [dbo].[Carrera_Asignatura]  WITH CHECK ADD  CONSTRAINT [FK_Carrera_Asignatura_Carrera] FOREIGN KEY([NoCarrera])
REFERENCES [dbo].[Carrera] ([NoCarrera])
GO
ALTER TABLE [dbo].[Carrera_Asignatura] CHECK CONSTRAINT [FK_Carrera_Asignatura_Carrera]
GO
ALTER TABLE [dbo].[Departamento]  WITH CHECK ADD  CONSTRAINT [fk_Facultad] FOREIGN KEY([IdFacultad])
REFERENCES [dbo].[Facultad] ([IdFacultad])
GO
ALTER TABLE [dbo].[Departamento] CHECK CONSTRAINT [fk_Facultad]
GO
ALTER TABLE [dbo].[Detalle_Matrícula]  WITH CHECK ADD FOREIGN KEY([NoAsignatura])
REFERENCES [dbo].[Asignatura] ([NoAsignatura])
GO
ALTER TABLE [dbo].[Detalle_Matrícula]  WITH CHECK ADD FOREIGN KEY([NoMatricula])
REFERENCES [dbo].[Matricula] ([NoMatrícula])
GO
ALTER TABLE [dbo].[Estudiante]  WITH CHECK ADD  CONSTRAINT [FK_Estudiante_Carrera] FOREIGN KEY([NoCarrera])
REFERENCES [dbo].[Carrera] ([NoCarrera])
GO
ALTER TABLE [dbo].[Estudiante] CHECK CONSTRAINT [FK_Estudiante_Carrera]
GO
ALTER TABLE [dbo].[Matricula]  WITH CHECK ADD  CONSTRAINT [FK_Matricula_Estudiante] FOREIGN KEY([IdEstudiante])
REFERENCES [dbo].[Estudiante] ([IdEstudiante])
GO
ALTER TABLE [dbo].[Matricula] CHECK CONSTRAINT [FK_Matricula_Estudiante]
GO
/****** Object:  StoredProcedure [dbo].[Buscar_Carrera]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Buscar_Carrera]
@Dato varchar(50)
as 
Select 
NoCarrera, 
IdFacultad, 
Nombre, 
Cantidad_Asignaturas,
Créditos
from Carrera 
where Nombre like @Dato + '%' 
GO
/****** Object:  StoredProcedure [dbo].[Buscar_Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Buscar_Estudiante]
	 @Dato varchar(20)
	 as
Select
 IdEstudiante as Id_Estudiante,
 [Primer Nombre],
 [Segundo Nombre],
 [Primer Apellido],
 [Segundo Apellido],
 Dirección,
 Teléfono,
 Correo
 from Estudiante
 where [Primer Nombre] like @Dato + '%' 
 or  [Segundo Nombre] like @Dato + '%' 
 or  [Primer Apellido] like @Dato + '%' 
  or  [Segundo Apellido] like @Dato + '%' 
GO
/****** Object:  StoredProcedure [dbo].[Buscar_Matricula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Buscar_Matricula]
	 @Dato int
	 as
Select
	m.[NoMatrícula] as [Número de Matrícula],
	e.[Primer Nombre] + ' '+ e.[Segundo Nombre] + ' ' + e.[Primer Apellido] + ' ' + e.[Segundo Apellido] as Nombre,
	e.IdEstudiante as ID,
	c.Nombre as [Nombre Carrera]
from Matricula m
inner join Estudiante e
on m.IdEstudiante = e.IdEstudiante
inner join Carrera c
on e.NoCarrera = c.NoCarrera
where [NoMatrícula] =  @Dato
GO
/****** Object:  StoredProcedure [dbo].[Buscar_Profesor]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Buscar_Profesor]
@Dato varchar(20)
as
Select
 [Id Profesor],
 [Primer Nombre],
 [Segundo Nombre],
 [Primer Apellido],
 [Segundo Apellido],
 Dirección,
 Teléfono
 from Profesor
 where [Primer Nombre] like @Dato + '%' 
 or  [Segundo Nombre] like @Dato + '%' 
 or  [Primer Apellido] like @Dato + '%' 
  or  [Segundo Apellido] like @Dato + '%' 
GO
/****** Object:  StoredProcedure [dbo].[Cambio_Estado_Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Cambio_Estado_Estudiante] 
@IdEstudiante int
as
Declare @Estado varchar(25)
set @Estado = (Select Estado from Estudiante where
                IdEstudiante = @IdEstudiante)

				if(@Estado = 'Activo')
				Begin
				update Estudiante set Estado = 'Inactivo'
				where IdEstudiante = @IdEstudiante
			    End
				Else
				Begin 
				update Estudiante set Estado = 'Activo'
				where IdEstudiante = @IdEstudiante
				End
GO
/****** Object:  StoredProcedure [dbo].[Editar_Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Editar_Estudiante]
	 @IdEstudiante int,
	 @NombreCarrera varchar(50),
	 @PrimerNombre varchar(15),
     @SegundoNombre varchar(15),
     @PrimerApellido varchar(15),
     @SegundoApellido varchar(15),
	 @Fecha_Ingreso Date,
	 @Direccion varchar(90),
	 @Telefono varchar(20),
	 @Correo varchar(40),
	 @Edad int
	 as
	 declare @Carrera int
	 set @Carrera = (Select c.NoCarrera from Carrera c
	 where @NombreCarrera = c.Nombre)
	 update Estudiante set
	  NoCarrera = @Carrera,
	  [Primer Nombre] =  @PrimerNombre,
	  [Segundo Nombre] = @SegundoNombre,
	 [Primer Apellido] = @PrimerApellido,
	 [Segundo Apellido] =  @SegundoApellido,
	 Fecha_Ingreso = @Fecha_Ingreso,
	 Dirección = @Direccion,
	 Teléfono = @Telefono,
	 Correo = @Correo,
	 Edad = @Edad
	 where IdEstudiante = @IdEstudiante
GO
/****** Object:  StoredProcedure [dbo].[Editar_Matricula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Editar_Matricula]
	 @NMatricula int,
	 @NombreEstudiante varchar(50),
	 @Año int
	 as
	 declare @Estudiante int
	 set @Estudiante = (Select e.IdEstudiante from Estudiante e
	 where @NombreEstudiante = e.[Primer Nombre]+' '+ e.[Segundo Nombre]+ ' '+
						 e.[Primer Apellido]+' '+e.[Segundo Apellido])
	 update Matricula set
	  IdEstudiante = @Estudiante,
	  Año =  @Año
	 where NoMatrícula = @NMatricula
GO
/****** Object:  StoredProcedure [dbo].[Editar_Profesor]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	 CREATE procedure [dbo].[Editar_Profesor]
	 @IdProfesor int,
	 @PrimerNombre varchar(15),
     @SegundoNombre varchar(15),
     @PrimerApellido varchar(15),
     @SegundoApellido varchar(15),
	 @Direccion varchar(90),
	 @Telefono varchar(20)
	 as
	 update Profesor set
	  [Primer Nombre] =  @PrimerNombre,
	  [Segundo Nombre] = @SegundoNombre,
	 [Primer Apellido] = @PrimerApellido,
	 [Segundo Apellido] =  @SegundoApellido,
	 Dirección = @Direccion,
	 Teléfono = @Telefono
	 where [Id Profesor] = @IdProfesor
GO
/****** Object:  StoredProcedure [dbo].[Inscripcion_Asignaturas]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Inscripcion_Asignaturas] @NoMatricula int, @Semestre int, @Aula int, @Grupo varchar(50)
as 
begin
	CREATE TABLE #IDAsignatura (idAsignatura int)

	insert into #IDAsignatura(idAsignatura) select NoAsignatura from Asignatura
	where Semestre = @Semestre
	
	Declare @numAsignaturas int = (
		select COUNT(*) from #IDAsignatura
	)

	while (@numAsignaturas > 0)
	begin 
		declare @NoAsignatura int = (select top 1 idAsignatura from #IDAsignatura)

		insert into Detalle_Matrícula values(@NoMatricula, @NoAsignatura, @Semestre, @Aula, @Grupo)

		delete #IDAsignatura where idAsignatura = @NoAsignatura

		set @numAsignaturas = (
			select COUNT(*) from #IDAsignatura
		)
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Insertar_Asignatura]
  @Nombre varchar(50),
   @NoCreditos int,
    @Semestre varchar(10),
	 @Clasificacion char(1)
	 as
	 Insert into Asignatura values (@Nombre,@NoCreditos,@Semestre,@Clasificacion)
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Detalle_Matricula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Insertar_Detalle_Matricula]
@NombreEstudiante varchar(80),
@NombreAsignatura varchar(80),
@Semestre int,
@Aula int,
@Grupo varchar(50)
as 
declare @NMatricula int
	 set @NMatricula = (Select m.NoMatrícula from Matricula m inner join Estudiante e on
	 m.IdEstudiante = e.IdEstudiante
	 where @NombreEstudiante = e.[Primer Nombre]+' '+ e.[Segundo Nombre]+ ' '+ e.[Primer Apellido]+' '+e.[Segundo Apellido])
declare @NAsignatura int
	 set @NAsignatura = (Select a.NoAsignatura from Asignatura a
	 where @NombreAsignatura = a.Nombre)
	 insert into Detalle_Matrícula values (@NMatricula, @NAsignatura, @Semestre, @Aula, @Grupo)
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Insertar_Estudiante]
@NombreCarrera varchar(80),
 @PrimerNombre varchar(15),
  @SegundoNombre varchar(15),
   @PrimerApellido varchar(15),
    @SegundoApellido varchar(15),
	 @Fecha_Ingreso Date,
	 @Direccion varchar(100),
	 @Teléfono int,
	 @Correo varchar(50),
	 @Edad int
	 as
	 declare @NoCarrera int
	 set @NoCarrera = (Select c.NoCarrera from Carrera c
	 where @NombreCarrera = c.Nombre)
	 insert into Estudiante values (@NoCarrera,@PrimerNombre,@SegundoNombre,@PrimerApellido, @SegundoApellido,
	    @Fecha_Ingreso, @Direccion, @Teléfono,@Correo,@Edad,'Activo')
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Matrícula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Insertar_Matrícula]
@NombreEstudiante varchar(80),
 @Año int
	 as
	 declare @IdEstudiante int
	 set @IdEstudiante = (Select e.IdEstudiante from Estudiante e
	 where @NombreEstudiante = e.[Primer Nombre]+' '+ e.[Segundo Nombre]+ ' '+ e.[Primer Apellido]+' '+e.[Segundo Apellido])
	 insert into Matricula values (@IdEstudiante,@Año)
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Profesor]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Insertar_Profesor]
 @PrimerNombre varchar(15),
  @SegundoNombre varchar(15),
   @PrimerApellido varchar(15),
    @SegundoApellido varchar(15),
	 @Direccion varchar(100),
	 @Teléfono int
	 as
	 insert into Profesor values (@PrimerNombre,@SegundoNombre,@PrimerApellido, @SegundoApellido,
	     @Direccion, @Teléfono)
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Usuario]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Insertar_Usuario] 
  @Usuario varchar(60),
  @Contraseña varchar(60),
  @Rol varchar(60)
  as

  Insert Into Usuario(usuario, contraseña, rol, estado)
   VALUES
  (@Usuario, ENCRYPTBYPASSPHRASE(@contraseña, @Contraseña), @Rol,'Habilitado')
GO
/****** Object:  StoredProcedure [dbo].[Mostrar_Carrera]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Mostrar_Carrera]
as 
Select NoCarrera, IdFacultad, Nombre, Cantidad_Asignaturas,Créditos
from Carrera
GO
/****** Object:  StoredProcedure [dbo].[Mostrar_DetalleMatrícula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[Mostrar_DetalleMatrícula]
as
Select dm.IdDetalleMatrícula, e.[Primer Nombre]+' '+ e.[Segundo Nombre]+' '+e.[Primer Apellido]+' '+e.[Segundo Apellido]
as [Nombre Estudiante], a.Nombre, dm.Semestre, dm.Aula, dm.Grupo
from Detalle_Matrícula dm inner join Matricula m
on dm.NoMatricula = m.NoMatrícula inner join Estudiante e
on m.IdEstudiante = e.IdEstudiante inner join Asignatura a
on dm.NoAsignatura = a.NoAsignatura
GO
/****** Object:  StoredProcedure [dbo].[Mostrar_Estudiante]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Mostrar_Estudiante]
as
Select
 e.IdEstudiante,
 c.Nombre as [Nombre Carrera],
 e.[Primer Nombre],
 e.[Segundo Nombre],
 e.[Primer Apellido],
 e.[Segundo Apellido],
 e.Fecha_Ingreso,
 e.Dirección,
 e.Teléfono,
 Correo,
 Edad,
 Estado
 from Estudiante e 
 inner join Carrera c on e.NoCarrera = c.NoCarrera
GO
/****** Object:  StoredProcedure [dbo].[Mostrar_Matricula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Mostrar_Matricula]
as  
Select
	m.[NoMatrícula] as [Número de Matrícula],
	e.[Primer Nombre] + ' '+ e.[Segundo Nombre] + ' ' + e.[Primer Apellido] + ' ' + e.[Segundo Apellido] as Nombre,
	e.IdEstudiante as ID,
	c.Nombre as [Nombre Carrera]
from Matricula m
inner join Estudiante e
on m.IdEstudiante = e.IdEstudiante
inner join Carrera c
on e.NoCarrera = c.NoCarrera
GO
/****** Object:  StoredProcedure [dbo].[Mostrar_Profesor]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Mostrar_Profesor]
as
Select
 [Id Profesor],
 [Primer Nombre],
 [Segundo Nombre],
 [Primer Apellido],
 [Segundo Apellido],
 [Dirección],
 [Teléfono]
 from Profesor
GO
/****** Object:  StoredProcedure [dbo].[MostrarAsignaturaPorSemestre]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MostrarAsignaturaPorSemestre]
	@Semestre int
AS
BEGIN
	Select a.NoAsignatura, a.Nombre,
	a.Semestre, a.Clasificacion, d.Nombre as [Nombre Departamento]
	from Asignatura a inner join Departamento d
	on a.NoDepartamento = d.NoDepartamento
	where a.Semestre = @Semestre	
END
GO
/****** Object:  StoredProcedure [dbo].[Validar_Acceso]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Validar_Acceso]
@Usuario varchar(60),
@Contraseña varchar(60)
as
Declare @Resultado varchar(50)
if exists (Select top 1 * from Usuario
           where Usuario = @Usuario and DECRYPTBYPASSPHRASE(@Contraseña, contraseña) = @Contraseña
		   and Estado = 'Habilitado')
		   Begin

		set @Resultado = 'Acceso Exitoso'
		end
		else
		set @Resultado = 'Acceso Denegado'
Select @Resultado as Resultado
GO
/****** Object:  StoredProcedure [Roleb1].[Buscar_Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [Roleb1].[Buscar_Asignatura]
	 @Dato varchar(20)
	 as
Select
 NoAsignatura, Nombre, NoCreditos, Semestre, Clasificacion
 from Asignatura
 where Nombre like @Dato + '%' 
 or  Semestre like @Dato + '%' 
 or  Clasificacion like @Dato + '%' 
GO
/****** Object:  StoredProcedure [Roleb1].[Editar_Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [Roleb1].[Editar_Asignatura]
	 @NoAsignatura int,
	 @Nombre varchar(50),
     @NoCreditos int,
     @Semestre varchar(15),
     @Clasificacion char(1)
	 as
	 update Asignatura set 
	  Nombre =  @Nombre,
	  NoCreditos = @NoCreditos,
	  Semestre = @Semestre,
	 Clasificacion =  @Clasificacion
	 where NoAsignatura = @NoAsignatura
GO
/****** Object:  StoredProcedure [Roleb1].[Mostrar_Asignatura]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Roleb1].[Mostrar_Asignatura]
as
Select
 a.NoAsignatura, a.Nombre, a.Semestre, a.Clasificacion, d.Nombre as [Nombre Departamento]
 from Asignatura a inner join Departamento d
 on a.NoDepartamento = d.NoDepartamento
GO
/****** Object:  StoredProcedure [Roleb1].[Mostrar_DetalleMatrícula]    Script Date: 01/12/2021 19:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [Roleb1].[Mostrar_DetalleMatrícula]
as
Select
 NoAsignatura, Semestre, Aula, Grupo, Estado
 from Detalle_Matrícula
GO
USE [master]
GO
ALTER DATABASE [Gestión_Matrícula] SET  READ_WRITE 
GO
