﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace proyecto_bd.model
{
    class MDetalleMatrícula
    {
        private int idDetalleMatrícula;
        private String NombreEstudiante;
        private String NombreAsignatura;
        private int semestre;
        private int aula;
        private String grupo;

        public int Id_Detalle_Matrícula { get => idDetalleMatrícula; set => idDetalleMatrícula = value; }
        public string N_Estudiante { get => NombreEstudiante; set => NombreEstudiante = value; }
        public string N_Asignatura{ get => NombreAsignatura; set => NombreAsignatura = value; }
        public int Semestre{ get => semestre; set => semestre = value; }
        public int Aula { get => aula; set => aula = value; }
        public string Grupo { get => grupo; set => grupo = value; }

        public static DataTable MostrarDetalleMatrícula()
        {
            DataTable DtResultado = new DataTable("MostrarDetalleMatrícula");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Mostrar_DetalleMatricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error");               
                DtResultado = null;
            }
            return DtResultado;
        }
        public static DataTable MostrarAsignaturas(int Semestre)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();

            try
            {
                SqlCon.ConnectionString = Conexion.Cn;

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "MostrarAsignaturaPorSemestre";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParSemestre = new SqlParameter();
                ParSemestre.ParameterName = "@Semestre";
                ParSemestre.SqlDbType = SqlDbType.Int;
                ParSemestre.Value = Semestre;
                SqlCmd.Parameters.Add(ParSemestre);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch
            {
                MessageBox.Show("Ha ocurrido un error" );
                DtResultado = null;
            }
            return DtResultado;
        }
        public string InsertarDetalleMatrícula(string NombreEstudiante, string NombreAsignatura, int semestre,
           int aula, string grupo)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Insertar_Detalle_Matricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParNombreEstudiante = new SqlParameter();
                ParNombreEstudiante.ParameterName = "@NombreEstudiante";
                ParNombreEstudiante.SqlDbType = SqlDbType.VarChar;
                ParNombreEstudiante.Size = 60;
                ParNombreEstudiante.Value = NombreEstudiante;
                SqlCmd.Parameters.Add(ParNombreEstudiante);

                SqlParameter ParNombreAsignatura = new SqlParameter();
                ParNombreAsignatura.ParameterName = "@NombreAsignatura";
                ParNombreAsignatura.SqlDbType = SqlDbType.VarChar;
                ParNombreAsignatura.Size = 60;
                ParNombreAsignatura.Value = NombreAsignatura;
                SqlCmd.Parameters.Add(ParNombreAsignatura);

                SqlParameter ParSemestre = new SqlParameter();
                ParSemestre.ParameterName = "@Semestre";
                ParSemestre.SqlDbType = SqlDbType.Int;
                ParSemestre.Value = semestre;
                SqlCmd.Parameters.Add(ParSemestre);

                SqlParameter ParAula = new SqlParameter();
                ParAula.ParameterName = "@Aula";
                ParAula.SqlDbType = SqlDbType.Int;
                ParAula.Value = aula;
                SqlCmd.Parameters.Add(ParAula);

                SqlParameter ParGrupo = new SqlParameter();
                ParGrupo.ParameterName = "@Grupo";
                ParGrupo.SqlDbType = SqlDbType.VarChar;
                ParGrupo.Size = 60;
                ParGrupo.Value = grupo;
                SqlCmd.Parameters.Add(ParGrupo);

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message + "a";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }
        public string EditarDetalleMatrícula(int IdDMatricula,string NombreEstudiante, string NombreAsignatura, int semestre,
           int aula, string grupo)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Editar_DMatricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParIDM = new SqlParameter();
                ParIDM.ParameterName = "IdDMatricula";
                ParIDM.SqlDbType = SqlDbType.Int;
                ParIDM.Value = IdDMatricula;
                SqlCmd.Parameters.Add(ParIDM);

                SqlParameter ParNombreEstudiante = new SqlParameter();
                ParNombreEstudiante.ParameterName = "@NombreEstudiante";
                ParNombreEstudiante.SqlDbType = SqlDbType.VarChar;
                ParNombreEstudiante.Size = 60;
                ParNombreEstudiante.Value = NombreEstudiante;
                SqlCmd.Parameters.Add(ParNombreEstudiante);

                SqlParameter ParNombreAsignatura = new SqlParameter();
                ParNombreAsignatura.ParameterName = "@NombreAsignatura";
                ParNombreAsignatura.SqlDbType = SqlDbType.VarChar;
                ParNombreAsignatura.Size = 60;
                ParNombreAsignatura.Value = NombreAsignatura;
                SqlCmd.Parameters.Add(ParNombreAsignatura);

                SqlParameter ParSemestre = new SqlParameter();
                ParSemestre.ParameterName = "@Semestre";
                ParSemestre.SqlDbType = SqlDbType.Int;
                ParSemestre.Value = semestre;
                SqlCmd.Parameters.Add(ParSemestre);

                SqlParameter ParAula = new SqlParameter();
                ParAula.ParameterName = "@Aula";
                ParAula.SqlDbType = SqlDbType.Int;
                ParAula.Value = aula;
                SqlCmd.Parameters.Add(ParAula);

                SqlParameter ParGrupo = new SqlParameter();
                ParGrupo.ParameterName = "@Grupo";
                ParGrupo.SqlDbType = SqlDbType.VarChar;
                ParGrupo.Size = 60;
                ParGrupo.Value = grupo;
                SqlCmd.Parameters.Add(ParGrupo);

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message + "a";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }
    }
}

