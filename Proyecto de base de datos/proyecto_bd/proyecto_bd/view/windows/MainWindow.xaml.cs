﻿using System;
using System.Windows;
using System.Windows.Controls;
using proyecto_bd.controller;

namespace proyecto_bd
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        MainController mainC;
        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }
        public void SetupController()
        {
            mainC = new MainController(this);
            this.Loaded += new RoutedEventHandler(mainC.MainWindowEventHandler);
            RoutedEventHandler routed = new RoutedEventHandler(mainC.MainMenuEventHandler);
            menuICatalogoEstudiante.Click += routed;
            //menuICatalogoCarrera.Click += routed;
            menuICatalogoAsignatura.Click += routed;
            menuICatalogoProfesor.Click += routed;

            menuIOpMatricula.Click += routed;
            menuIReporteAño.Click += routed;
        }

    }
}
