﻿using proyecto_bd.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para CatAsignaturaUserControl.xaml
    /// </summary>
    public partial class CatAsignaturaUserControl : UserControl
    {
        private bool IsNuevo = false;
        private bool IsEditar = false;

        public CatAsignaturaUserControl()
        {
            InitializeComponent();
            Botones();
            mostrarAsignatura();
            pnlBuscar.btnBuscar.Click += new RoutedEventHandler(BtnBuscar_Click);
            pnlBuscar.btnCancelar.Click += new RoutedEventHandler(BtnCancelar_Click);
        }

        public void mostrarAsignatura()
        {
            dtGAsignatura.DataContext = CAsignatura.MostrarAsignatura();
        }
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar) //Alt + 124
            {
                this.Habilitar(true);
                this.btnNuevo.IsEnabled = false;
                this.btnGuardar.IsEnabled = true;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = false;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = false;
            }

        }

        private void Habilitar(bool valor)
        {
            this.txtNombre.IsReadOnly = !valor;
            this.txtCreditos.IsReadOnly = !valor;
            this.txtSemestre.IsReadOnly = !valor;
            this.txtClasificacion.IsReadOnly = !valor;
        }

        private void Limpiar()
        {
            this.txtNombre.Text = string.Empty;
            this.txtCreditos.Text = string.Empty;
            this.txtSemestre.Text = string.Empty;
            this.txtClasificacion.Text = string.Empty;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
           
            txtCreditos.IsEnabled = false;
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.txtNombre.Focus();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rpta = "";

                if (this.IsNuevo) { 

                    MessageBox.Show("Ingresando Asignatura");
                    //rpta = CEstudiante.InsertarEstudiante(this.txtPrimerNombre.Text, this.txtSegundoNombre.Text, this.txtPrimerApellido.Text, this.txtSegundoApellido.Text, fecha, this.txtDireccion.Text, Int32.Parse( this.txtTelefono.Text), this.txtCorreo.Text, 20);
                    rpta = CAsignatura.InsertarAsignatura(txtNombre.Text, 3, txtSemestre.Text,char.Parse(txtClasificacion.Text));
                    //(txtPrimerNombre.Text, txtSegundoNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, fecha, txtDireccion.Text, Int32.Parse(this.txtTelefono.Text), txtCorreo.Text, 20);

                    //rpta = CAsignatura.InsertarAsignatura(txtNombre.Text, int.Parse(txtCreditos.Text), txtSemestre.Text,char.Parse(txtClasificacion.Text));
                    //      3c31d7ac1f004061237614e33995602f2401a680

                }
                else
                {
                    DataRowView row_selected = dtGAsignatura.SelectedItem as DataRowView;
                    rpta = CAsignatura.EditarAsignatura(Convert.ToInt32(row_selected["No_Asignatura"].ToString()),
                        this.txtNombre.Text, 3,this.txtSemestre.Text, Convert.ToChar(this.txtClasificacion.Text));
                    mostrarAsignatura();
                }

                if (rpta.Equals("OK"))
                {
                    if (this.IsNuevo)
                    {

                        MessageBox.Show("Datos Ingresados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos Actualizados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {

                    MessageBox.Show(rpta, "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                this.IsNuevo = false;
                this.IsEditar = false;
                this.Botones();
                mostrarAsignatura();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }

        }

        private void btnMostrar_Click(object sender, RoutedEventArgs e)
        {
            mostrarAsignatura();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.IsNuevo = false;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();

        }
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            txtCreditos.IsEnabled = false;
            DataRowView row_selected = dtGAsignatura.SelectedItem as DataRowView;
            if (this.dtGAsignatura.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    txtNombre.Text = row_selected["Nombre"].ToString();
                    txtCreditos.Text = row_selected["NoCreditos"].ToString();
                    txtSemestre.Text = row_selected["Semestre"].ToString();
                    txtClasificacion.Text = row_selected["Clasificacion"].ToString();
                    MessageBox.Show("" + row_selected.GetHashCode());
                    this.IsNuevo = false;
                    this.IsEditar = true;
                    this.Botones();
                    this.txtNombre.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila antes de Modificar", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
        }

        private void pnlBuscar_Loaded(object sender, RoutedEventArgs e)
        {
            this.dtGAsignatura.DataContext = CAsignatura.MostrarAsignatura();
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            if (pnlBuscar.txtBuscar.Text != string.Empty)
            {
                dtGAsignatura.DataContext = CAsignatura.BuscarAsignatura(pnlBuscar.txtBuscar.Text);
            }
            else
            {
                MessageBox.Show("No hay nada para Buscar ");
            }

        }
        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (pnlBuscar.txtBuscar.Text != string.Empty)
            {
                dtGAsignatura.DataContext = CAsignatura.MostrarAsignatura();
                pnlBuscar.txtBuscar.Text = string.Empty;
            }
        }
    }
}
