﻿using proyecto_bd.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para CatEstudianteUserControl.xaml
    /// </summary>
    public partial class CatEstudianteUserControl : UserControl
    {
        private bool IsNuevo = false;
        private bool IsEditar = false;
        public CatEstudianteUserControl()
        {
            InitializeComponent();
            Botones();
            mostrarEstudiante();
        }
        public void mostrarEstudiante()
        {
            dtGEstudiante.DataContext = CEstudiante.MostrarEstudiante();
        }
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            IsNuevo = true;
            Botones();
            Limpiar();
        }
        public void setTxtCarrera(string nombre)
        {
            this.txtCarrera.Text = nombre;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtCarrera.IsEnabled = false;
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rpta = "";

                if (this.IsNuevo)
                {               
                    DateTime fecha = DateTime.Today;
                    rpta = CEstudiante.InsertarEstudiante(this.txtCarrera.Text,this.txtPrimerNombre.Text, this.txtSegundoNombre.Text,
                        this.txtPrimerApellido.Text, this.txtSegundoApellido.Text, fecha, this.txtDireccion.Text, 
                        Int32.Parse( this.txtTelefono.Text), 
                        this.txtCorreo.Text, Convert.ToInt32(txtEdad.Text));
                    //rpta = CEstudiante.InsertarEstudiante(txtPrimerNombre.Text, txtSegundoNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, fecha, txtDireccion.Text, Int32.Parse(this.txtTelefono.Text), txtCorreo.Text,20);

                }
                else
                {
                    DateTime fecha = DateTime.Today;
                    DataRowView row_selected = dtGEstudiante.SelectedItem as DataRowView;
                    rpta = CEstudiante.EditarEstudiante(Convert.ToInt32(row_selected["IdEstudiante"].ToString()),
                        this.txtCarrera.Text, this.txtPrimerNombre.Text, this.txtSegundoNombre.Text,
                        this.txtPrimerApellido.Text, this.txtSegundoApellido.Text, fecha,
                        this.txtDireccion.Text, Convert.ToInt32(this.txtTelefono.Text), txtCorreo.Text, Convert.ToInt32(txtEdad.Text));
                }
                if (rpta.Equals("OK"))
                {
                    if (this.IsNuevo)
                    {

                        MessageBox.Show("Datos Ingresados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos Actualizados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show(rpta, "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                this.IsNuevo = false;
                this.IsEditar = false;
                this.Botones();
                this.Limpiar();
                mostrarEstudiante();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }          
        }
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar) //Alt + 124
            {
                this.Habilitar(true);
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = true;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = true;
                this.btnEstado.IsEnabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = false;
                this.btnModificar.IsEnabled = false;
                this.btnCancelar.IsEnabled = false;
                this.btnEstado.IsEnabled = true;
            }
        }
        private void Habilitar(bool valor)
        {
            btnBuscarCarrera.IsEnabled = valor;
            this.txtPrimerNombre.IsReadOnly = !valor;
            this.txtSegundoNombre.IsReadOnly = !valor;
            this.txtPrimerApellido.IsReadOnly = !valor;
            this.txtSegundoApellido.IsReadOnly = !valor;
            this.txtDireccion.IsReadOnly = !valor;
            this.txtCorreo.IsReadOnly = !valor;
            this.txtTelefono.IsReadOnly = !valor;
        }
        private void Limpiar()
        {
            this.txtCarrera.Text = string.Empty;
            this.txtPrimerNombre.Text = string.Empty;
            this.txtSegundoNombre.Text = string.Empty;
            this.txtPrimerApellido.Text = string.Empty;
            this.txtSegundoApellido.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtCorreo.Text = string.Empty;
            this.txtTelefono.Text = string.Empty;
            this.txtEdad.Text = string.Empty;
        }
        private void btnBuscarCarrera_Click(object sender, RoutedEventArgs e)
        {
            BuscarNombreCarrera bnc = new BuscarNombreCarrera(this);
            bnc.ShowDialog();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGEstudiante.SelectedItem as DataRowView;
            if (this.dtGEstudiante.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    txtCarrera.Text = row_selected["Nombre Carrera"].ToString();
                    txtPrimerNombre.Text = row_selected["Primer Nombre"].ToString();
                    txtSegundoNombre.Text = row_selected["Segundo Nombre"].ToString();
                    txtPrimerApellido.Text = row_selected["Primer Apellido"].ToString();
                    txtSegundoApellido.Text = row_selected["Segundo Apellido"].ToString();
                    txtDireccion.Text = row_selected["Dirección"].ToString();
                    txtTelefono.Text = row_selected["Teléfono"].ToString();
                    txtCorreo.Text = row_selected["Correo"].ToString();
                    txtEdad.Text = row_selected["Edad"].ToString();
                    MessageBox.Show("" + row_selected.GetHashCode());
                    this.IsNuevo = false;
                    this.IsEditar = true;
                    this.Botones();
                    this.txtPrimerNombre.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila antes de Modificar", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnEstado_Click(object sender, RoutedEventArgs e)
        {
            if (this.dtGEstudiante.SelectedItems.Count == 1)
            {
                try
                {
                    DataRowView row_selected = dtGEstudiante.SelectedItem as DataRowView;
                    CEstudiante.CambioEstado(Convert.ToInt32(row_selected["IdEstudiante"].ToString()));
                    MessageBox.Show("El estado ha sido actualizado", "Gestión Matrícula");

                    this.IsNuevo = false;
                    this.IsEditar = false;
                    this.Botones();
                    this.Limpiar();
                    this.dtGEstudiante.DataContext = CEstudiante.MostrarEstudiante();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace);
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para cambiar el estado", "Sistema de Reservas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

    }
}
