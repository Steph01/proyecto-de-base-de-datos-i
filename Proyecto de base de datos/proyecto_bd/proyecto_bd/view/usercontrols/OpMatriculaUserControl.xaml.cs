﻿using proyecto_bd.controller;
using proyecto_bd.viewmodel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para OpMatriculaUserControl.xaml
    /// </summary>
    public partial class OpMatriculaUserControl : UserControl
    {
        public OpMatriculaUserControl()
        {
            
            InitializeComponent();
            mostrarRegistrosMatricula();
            DataContext = new MatriculaViewModel();

        }

        public void mostrarRegistrosMatricula()
        {
            //dtGMatricula.DataContext = CMatricula.MostrarMatricula();
            
        }


        private void lblMatricula_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DataContext = new MatriculaViewModel();
        }

        private void lblInscripcion_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DataContext = new InscripcionViewModel();
        }
    }
}
