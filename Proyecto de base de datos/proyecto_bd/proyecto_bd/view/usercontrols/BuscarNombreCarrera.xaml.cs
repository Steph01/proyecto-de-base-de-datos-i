﻿using proyecto_bd.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para BuscarNombreCarrera.xaml
    /// </summary>
    public partial class BuscarNombreCarrera : Window
    {
        public static string nombre;
        private CatEstudianteUserControl ce;
        public BuscarNombreCarrera(CatEstudianteUserControl ce)
        {
            InitializeComponent();
            mostrarCarrera();
            this.ce = ce;
        }
        public void mostrarCarrera()
        {
            dtGCarrera.DataContext = CCarrera.MostrarCarrera();
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGCarrera.SelectedItem as DataRowView;
            if (this.dtGCarrera.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    nombre = row_selected["Nombre"].ToString();
                    ce.txtCarrera.Text = nombre;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void txtNombreCarrera_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.dtGCarrera.DataContext = CCarrera.BuscarCarrera(this.txtNombreCarrera.Text);
        }
    }
}
