﻿using proyecto_bd.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para MatriculaUserControl.xaml
    /// </summary>
    public partial class MatriculaUserControl : UserControl
    {

        private bool IsNuevo = false;
        private bool IsEditar = false;
        public MatriculaUserControl()
        {
            InitializeComponent();
            Botones();
            mostrarRegistrosMatricula();
        }
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar) //Alt + 124
            {
                this.btnNuevo.IsEnabled = false;
                this.btnGuardar.IsEnabled = true;
                this.btnModificar.IsEnabled = false;
                this.btnCancelar.IsEnabled = true;
                btnBuscarEstudiante.IsEnabled = true;
            }
            else
            {
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = false;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = false;
                btnBuscarEstudiante.IsEnabled = false;
            }
        }

        private void mostrarRegistrosMatricula()
        {
            dtGMatricula.DataContext = CMatricula.MostrarMatricula();
        }
        private void Limpiar()
        {
            this.txtNombreEstudiante.Text = string.Empty;
            this.txtAño.Text = string.Empty;
            this.txtID.Text = string.Empty;
        }

        private void BuscarEstudiante_Click(object sender, RoutedEventArgs e)
        {
            BuscarNombreEstudiante bnc = new BuscarNombreEstudiante(this);
            bnc.ShowDialog();
        }
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGMatricula.SelectedItem as DataRowView;
            if (this.dtGMatricula.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    this.txtNombreEstudiante.Text = row_selected["Nombre"].ToString();
                    this.txtAño.Text = row_selected["Año"].ToString();
                    

                    IsNuevo = false;
                    IsEditar = true;
                    Botones();
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna matrícula.", 
                    "Sistema de Gestión de matrículas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rpta = "";

                if (this.IsNuevo)
                {
                    rpta = CMatricula.InsertarMatricula(this.txtNombreEstudiante.Text,
                        Convert.ToInt32(this.txtAño.Text));
                }
                else
                {
                    DataRowView row_selected = dtGMatricula.SelectedItem as DataRowView;
                    rpta = CMatricula.EditarMatricula(Convert.ToInt32(row_selected["Número de Matrícula"].ToString()),
                        this.txtNombreEstudiante.Text,Convert.ToInt32(this.txtAño.Text));
                }
                if (rpta.Equals("OK"))
                {
                    if (this.IsNuevo)
                    {
                        MessageBox.Show("Datos Ingresados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos Actualizados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show(rpta, "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                this.IsNuevo = false;
                this.IsEditar = false;
                this.Botones();
                this.Limpiar();
                mostrarRegistrosMatricula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            IsNuevo = false;
            IsEditar = false;

            Botones();
            Limpiar();
        }
    }
}
