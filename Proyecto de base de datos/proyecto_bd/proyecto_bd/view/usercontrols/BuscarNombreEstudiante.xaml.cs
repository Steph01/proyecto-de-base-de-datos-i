﻿using proyecto_bd.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace proyecto_bd.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para BuscarNombreEstudiante.xaml
    /// </summary>
    public partial class BuscarNombreEstudiante : Window
    {
        
        public static string nombre;
        public static string id;
        public static string Carrera;

        private MatriculaUserControl cm;
        public BuscarNombreEstudiante(MatriculaUserControl cm)
        {
            InitializeComponent();
            mostrarEstudiante();
            this.cm = cm;
        }
        public void mostrarEstudiante()
        {
            dtGEstudiante.DataContext = CEstudiante.MostrarEstudiante();
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            
            DataRowView row_selected = dtGEstudiante.SelectedItem as DataRowView;
            if (this.dtGEstudiante.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    nombre = row_selected["Primer Nombre"].ToString() +" "+ row_selected["Segundo Nombre"].ToString() + " " +
                        row_selected["Primer Apellido"].ToString() + " "+ row_selected["Segundo Apellido"].ToString();
                    cm.txtNombreEstudiante.Text = nombre;
                    cm.txtID.Text = id;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void txtNombreEstudiante_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.dtGEstudiante.DataContext = CEstudiante.BuscarEstudiante(this.txtNombreEstudiante.Text);
        }
    }
}
