﻿using proyecto_bd.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using proyecto_bd.model;

namespace proyecto_bd.controller
{
    class CDetalleMatrícula
    {
        public static DataTable MostrarAsignaturas(int semestre)
        {
            return MDetalleMatrícula.MostrarAsignaturas(semestre);
        }
        public static string Insertar_DetalleMatricula(string NombreEstudiante, string NombreAsignatura, 
            int semestre, int aula, string grupo)
        {
            MDetalleMatrícula Obj = new MDetalleMatrícula();
            return  Obj.InsertarDetalleMatrícula(NombreEstudiante, NombreAsignatura, semestre, aula, grupo);
        }
        public static DataTable MostrarDMatrícula()
        {
            return MDetalleMatrícula.MostrarDetalleMatrícula();
        }
        public static string EditarDetalleMatrícula(int IdDMatricula,string NombreEstudiante, string NombreAsignatura, int semestre,
           int aula, string grupo)
        {
            MDetalleMatrícula Obj = new MDetalleMatrícula();

            return Obj.EditarDetalleMatrícula(IdDMatricula,NombreEstudiante, NombreAsignatura, semestre,aula,grupo);
        }

    }
}
