﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using proyecto_bd.model;

namespace proyecto_bd.controller
{
    class CEstudiante
    {
        public static DataTable CambioEstado(int idEstudiante)
        {
            return MEstudiante.CambioEstado(idEstudiante);
        }
        public static DataTable MostrarEstudiante()
        {
            return MEstudiante.MostrarEstudiante();
        }

        public static string InsertarEstudiante(string nombreCarrera, string p_nombre, string s_nombre, string p_apellido, string s_apellido,
            DateTime FechaIngreso, string Direccion, int Telefono, string Correo, int Edad)
        {
            MEstudiante Obj = new MEstudiante();

            return Obj.InsertarEstudiante(nombreCarrera, p_nombre, s_nombre, p_apellido, s_apellido,
                FechaIngreso, Direccion, Telefono, Correo, Edad);
        }
        public static string EditarEstudiante(int idEstudiante, string nombreCarrera,string p_nombre, string s_nombre, string p_apellido, string s_apellido,
            DateTime FechaIngreso, string Direccion, int Telefono, string Correo, int Edad)
        {
            MEstudiante Obj = new MEstudiante();

            return Obj.EditarEstudiante(idEstudiante, nombreCarrera, p_nombre, s_nombre,
                p_apellido, s_apellido, FechaIngreso, Direccion, Telefono, Correo, Edad);
        }
        public static DataTable BuscarEstudiante(string dato)
        {
            return MEstudiante.BuscarEstudiante(dato);
        }
    }
}
