﻿using proyecto_bd.view;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;

namespace proyecto_bd.controller
{
    
    public class LoginController
    {
        LoginWindow login;
        public LoginController(LoginWindow mainWindow)
        {
            login = mainWindow;
            
        }

        public void accionBotones(string nombreBotones)
        {
            
            if (nombreBotones.Equals("btnAceptar"))
            {
                btnAceptarClick();
            }

            if(nombreBotones.Equals("btnCancelar"))
            {
                btnCancelarClick();
            }
        }


        private void btnAceptarClick()
        {
            
            DataTable dato;
          dato = CUsuario.Validar_Acceso(login.txtUsuario.Text, login.txtContraseña.Password);

            if (dato != null)
            {

                if (dato.Rows.Count > 0)
                {
                    DataRow dr;
                    dr = dato.Rows[0];

                    if (dr["Resultado"].ToString() == "Acceso Exitoso")
                    {
                        //rol = dato.Rows[0][3].ToString();
                        //nombre = dato.Rows[0][2].ToString();
                        //idEmpleado = int.Parse(dato.Rows[0][1].ToString());


                        MessageBox.Show("Bienvenido al Sistema", "Sistema de Matricula", MessageBoxButton.OK, MessageBoxImage.Information);
                        /* Form1 fc = new Form1();
                         fc.Show();
                         this.Hide();*/

                        login.Hide();
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Show();

                    }
                    else
                    {
                        MessageBox.Show("Acceso Denegado a la Gestion de matricula", "Sistema de Matricula", MessageBoxButton.OK, MessageBoxImage.Error);
                        login.txtUsuario.Text = string.Empty;
                        login.txtContraseña.Password = string.Empty;
                        login.txtUsuario.Focus();
                    }
                }
            }
            else
                MessageBox.Show("No hay conexión al servidor", "Sistema de Matricula", MessageBoxButton.OK, MessageBoxImage.Error);
           
        }

        private void btnCancelarClick()
        {
            MessageBox.Show("Cerrando Aplicación");
            Environment.Exit(0);
        }

    }
}
