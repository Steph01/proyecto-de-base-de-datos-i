﻿using proyecto_bd.model;
using System.Data;

namespace proyecto_bd.controller
{
    class CAsignatura
    {
        public static DataTable MostrarAsignatura()
        {
            return MAsignatura.MostrarAsignatura();
        }

        public static DataTable BuscarAsignatura(string dato)
        {
            return MAsignatura.BuscarAsignatura(dato);
        }

        public static string InsertarAsignatura(string NombreA, int Creditos, string Semestre, char Clasificacion)
        {
            MAsignatura Obj = new MAsignatura();

            return Obj.InsertarAsignatura(NombreA, Creditos, Semestre, Clasificacion);
        }
        public static string EditarAsignatura(int NoAsignatura, string NombreA, int Creditos, string Semestre, char Clasificacion)
        {
            MAsignatura Obj = new MAsignatura();

            return Obj.EditarAsignatura(NoAsignatura, NombreA, Creditos, Semestre, Clasificacion);
        }

    }
}
