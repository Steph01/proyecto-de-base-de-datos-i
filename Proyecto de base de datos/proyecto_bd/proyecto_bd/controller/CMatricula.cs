﻿using proyecto_bd.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace proyecto_bd.controller
{
    class CMatricula
    {
        public static DataTable MostrarMatricula()
        {
            return MMatricula.MostrarMatricula();
        }

        public static string InsertarMatricula(string NombreEstudiante,int Año)
        {
            MMatricula Obj = new MMatricula();

            return Obj.InsertarMatricula(NombreEstudiante, Año);
        }
        public static string EditarMatricula(int NoMatricula, string NombreEstudiante, int Año)
        {
            MMatricula Obj = new MMatricula();

            return Obj.EditarMatricula(NoMatricula, NombreEstudiante, Año);
        }

        public static DataTable BuscarMatricula(string idMatricula)
        {
            return MMatricula.BuscarMatricula(int.Parse(idMatricula));
        }
    }
}
