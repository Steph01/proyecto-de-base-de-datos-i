﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using proyecto_bd.viewmodel;

namespace proyecto_bd.controller
{
    class MainController
    {
        MainWindow mainw;
        public MainController(MainWindow mainWindow)
        {
            mainw = mainWindow;
        }

        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                
                case "menuICatalogoEstudiante":
                    mainw.DataContext = new CatEstudianteViewModel();
                    break;
                /*case "menuICatalogoCarrera":
                    mainw.DataContext = new CatCarreraViewModel();
                    break;*/
                case "menuICatalogoAsignatura":
                    mainw.DataContext = new CatAsignaturaViewModel();
                    break;
                case "menuICatalogoProfesor":
                    mainw.DataContext = new CatProfesorViewModel();
                    break;
                case "menuIOpMatricula":
                    mainw.DataContext = new OpMatriculaViewModel();
                    break;
                case "menuIReporteAño":
                    mainw.DataContext = new ReportePorAño();
                    break;
                case "menuIReporteAsignatura":
                    //mainw.DataContext = new R;
                    break;
            }
            
        }
    }
}
