﻿using proyecto_bd.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace proyecto_bd.controller
{
    class CCarrera
    {
        public static DataTable MostrarCarrera()
        {
            return MCarrera.MostrarCarrera();
        }

        public static DataTable BuscarCarrera(string dato)
        {
            return MCarrera.BuscarCarrera(dato);
        }

    }
}
