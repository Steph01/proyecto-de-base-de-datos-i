﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace proyecto_bd.reports
{
    /// <summary>
    /// Lógica de interacción para ReporteAlumnosPorAño.xaml
    /// </summary>
    public partial class ReporteAlumnosPorAño : UserControl
    {
        public ReporteAlumnosPorAño()
        {
            InitializeComponent();
            ReportViewer.ReportPath = System.IO.Path.Combine(Environment.CurrentDirectory, @"reports\AlumnosMatriculados.rdlc"); 
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
