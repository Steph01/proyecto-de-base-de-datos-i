﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using ProyectBD_GM.view.windows;
using ProyectBD_GM.viewmodel;

namespace ProyectBD_GM.controller
{
    class MainController
    {
        MainWindow mainw;
        public MainController(MainWindow mainWindow)
        {
            mainw = mainWindow;
        }

        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                
                case "menuICatalogoEstudiante":
                    mainw.DataContext = new CatEstudianteViewModel();
                    break;
                case "menuICatalogoAsignatura":
                    mainw.DataContext = new CatAsignaturaViewModel();
                    break;
                case "menuICatalogoProfesor":
                    mainw.DataContext = new CatProfesorViewModel();
                    break;
                case "menuIOpMatricula":
                    mainw.DataContext = new OpMatriculaViewModel();
                    break;
                case "menuIReporteAño":
                    ProyectBD_GM.Reportes.ReportesAño frmapp = new ProyectBD_GM.Reportes.ReportesAño();
                    frmapp.Show();
                    break;
                case "menuIReporteAsignatura":
                    ProyectBD_GM.Reportes.ReportesAsignatura frmapp2 = new ProyectBD_GM.Reportes.ReportesAsignatura();
                    frmapp2.Show();
                    break;
                case "menuIReporteGrupo":
                    ProyectBD_GM.Reportes.ReportesGrupo frmapp3 = new ProyectBD_GM.Reportes.ReportesGrupo();
                    frmapp3.Show();
                    break;
                case "menuIReporteDetalleMatricula":
                    ProyectBD_GM.Reportes.ReporteDMatricula frmapp4 = new ProyectBD_GM.Reportes.ReporteDMatricula();
                    frmapp4.Show();
                    break;
            }
            
        }
    }
}
