﻿using ProyectBD_GM.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ProyectBD_GM.controller
{
    class CCarrera
    {
        public static DataTable MostrarCarrera()
        {
            return MCarrera.MostrarCarrera();
        }

        public static DataTable BuscarCarrera(string dato)
        {
            return MCarrera.BuscarCarrera(dato);
        }

    }
}
