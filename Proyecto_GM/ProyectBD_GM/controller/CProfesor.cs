﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProyectBD_GM.model;

namespace ProyectBD_GM.controller
{
    class CProfesor
    {
        public static DataTable MostrarProfesor()
        {
            return MProfesor.MostrarProfesor();
        }
        public static DataTable BuscarProfesor(string dato)
        {
            return MProfesor.BuscarProfesor(dato);
        }

        public static string InsertarProfesor(string p_nombre, string s_nombre, string p_apellido, string s_apellido,
           string direccion, int telefono)
        {
            MProfesor Obj = new MProfesor();

            return Obj.InsertarProfesor(p_nombre, s_nombre, p_apellido, s_apellido,
                direccion, telefono);
        }
        public static string EditarProfesor(int idProfesor, string p_nombre, string s_nombre, string p_apellido, string s_apellido,
            string direccion, int telefono)
        {
            MProfesor Obj = new MProfesor();

            return Obj.EditarProfesor(idProfesor, p_nombre, s_nombre,
                p_apellido, s_apellido, direccion, telefono);
        }
    }
}
