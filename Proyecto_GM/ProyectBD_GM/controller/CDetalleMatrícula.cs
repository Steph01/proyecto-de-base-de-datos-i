﻿using ProyectBD_GM.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace ProyectBD_GM.controller
{
    class CDetalleMatrícula
    {
        public static DataTable MostrarAsignaturas(int semestre)
        {
            return MDetalleMatrícula.MostrarAsignaturas(semestre);
        }
        public static string Insertar_DetalleMatricula(string NombreEstudiante, string NombreAsignatura, 
            int semestre, int aula, string grupo)
        {
            MDetalleMatrícula Obj = new MDetalleMatrícula();
            return  Obj.InsertarDetalleMatrícula(NombreEstudiante, NombreAsignatura, semestre, aula, grupo);
        }
        public static DataTable MostrarDMatrícula()
        {
            return MDetalleMatrícula.MostrarDetalleMatrícula();
        }
        public static string EditarDetalleMatrícula(int IdDMatricula,string NombreEstudiante, string NombreAsignatura, int semestre,
           int aula, string grupo)
        {
            MDetalleMatrícula Obj = new MDetalleMatrícula();

            return Obj.EditarDetalleMatrícula(IdDMatricula,NombreEstudiante, NombreAsignatura, semestre,aula,grupo);
        }

    }
}
