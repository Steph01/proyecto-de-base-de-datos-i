﻿
namespace ProyectBD_GM.Reportes
{
    partial class ReportesAsignatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.AlumnosMatriculadosAsignaturaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataRepAño = new ProyectBD_GM.Reportes.DataRepAño();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AlumnosMatriculadosAsignaturaTableAdapter = new ProyectBD_GM.Reportes.DataRepAñoTableAdapters.AlumnosMatriculadosAsignaturaTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.AlumnosMatriculadosAñoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AlumnosMatriculadosAñoTableAdapter = new ProyectBD_GM.Reportes.DataRepAñoTableAdapters.AlumnosMatriculadosAñoTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAsignaturaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataRepAño)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAñoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // AlumnosMatriculadosAsignaturaBindingSource
            // 
            this.AlumnosMatriculadosAsignaturaBindingSource.DataMember = "AlumnosMatriculadosAsignatura";
            this.AlumnosMatriculadosAsignaturaBindingSource.DataSource = this.DataRepAño;
            // 
            // DataRepAño
            // 
            this.DataRepAño.DataSetName = "DataRepAño";
            this.DataRepAño.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource5.Name = "DataSet1";
            reportDataSource5.Value = this.AlumnosMatriculadosAsignaturaBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource5);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ProyectBD_GM.Reportes.ReporteAsignatura.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 73);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 377);
            this.reportViewer1.TabIndex = 0;
            // 
            // AlumnosMatriculadosAsignaturaTableAdapter
            // 
            this.AlumnosMatriculadosAsignaturaTableAdapter.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(300, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Location = new System.Drawing.Point(435, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "BUSCAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AlumnosMatriculadosAñoBindingSource
            // 
            this.AlumnosMatriculadosAñoBindingSource.DataMember = "AlumnosMatriculadosAño";
            this.AlumnosMatriculadosAñoBindingSource.DataSource = this.DataRepAño;
            // 
            // AlumnosMatriculadosAñoTableAdapter
            // 
            this.AlumnosMatriculadosAñoTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(188, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "ASIGNATURA";
            // 
            // ReportesAsignatura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ReportesAsignatura";
            this.Text = "ReportesAsignatura";
            this.Load += new System.EventHandler(this.ReportesAsignatura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAsignaturaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataRepAño)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAñoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource AlumnosMatriculadosAsignaturaBindingSource;
        private DataRepAño DataRepAño;
        private DataRepAñoTableAdapters.AlumnosMatriculadosAsignaturaTableAdapter AlumnosMatriculadosAsignaturaTableAdapter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource AlumnosMatriculadosAñoBindingSource;
        private DataRepAñoTableAdapters.AlumnosMatriculadosAñoTableAdapter AlumnosMatriculadosAñoTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}