﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectBD_GM.Reportes
{
    public partial class ReporteDMatricula : Form
    {
        public ReporteDMatricula()
        {
            InitializeComponent();
            this.reportViewer1.RefreshReport();
        }

        private void ReporteDMatricula_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'DataRepAño.ReporteDetalleMatrícula' Puede moverla o quitarla según sea necesario.
            this.ReporteDetalleMatrículaTableAdapter.Fill(this.DataRepAño.ReporteDetalleMatrícula);

            this.reportViewer1.RefreshReport();
        }
    }
}
