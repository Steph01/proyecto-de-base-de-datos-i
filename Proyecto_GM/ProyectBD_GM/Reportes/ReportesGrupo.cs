﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectBD_GM.Reportes
{
    public partial class ReportesGrupo : Form
    {
        public ReportesGrupo()
        {
            InitializeComponent();
        }

        private void ReportesGrupo_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'DataRepAño.AlumnosPorGrupo' Puede moverla o quitarla según sea necesario.
            this.reportViewer1.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.AlumnosPorGrupoTableAdapter.Fill(this.DataRepAño.AlumnosPorGrupo,this.textBox1.Text);

            this.reportViewer1.RefreshReport();
        }
    }
}
