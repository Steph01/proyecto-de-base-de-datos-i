﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectBD_GM.Reportes
{
    public partial class ReportesAño : Form
    {
        public ReportesAño()
        {
            InitializeComponent();
        }

        private void ReportesAño_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'DataRepAño.AlumnosMatriculadosAño' Puede moverla o quitarla según sea necesario.
            
            // TODO: esta línea de código carga datos en la tabla 'DataRepAño.AlumnosMatriculadosAsignatura' Puede moverla o quitarla según sea necesario.

            this.reportViewer1.RefreshReport();
        }

        private void buscar_Click(object sender, EventArgs e)
        {
            this.AlumnosMatriculadosAñoTableAdapter.Fill(this.DataRepAño.AlumnosMatriculadosAño, int.Parse(this.txtaño.Text));

            this.reportViewer1.RefreshReport();
        }

       
    }
}
