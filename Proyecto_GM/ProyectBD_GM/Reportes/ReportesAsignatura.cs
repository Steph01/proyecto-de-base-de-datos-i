﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectBD_GM.model;

namespace ProyectBD_GM.Reportes
{
    public partial class ReportesAsignatura : Form
    {
        public ReportesAsignatura()
        {
            InitializeComponent();
        }

        private void ReportesAsignatura_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.AlumnosMatriculadosAsignaturaTableAdapter.Fill(this.DataRepAño.AlumnosMatriculadosAsignatura, this.textBox1.Text);
            this.reportViewer1.RefreshReport();
        }
    }

        
    }

