﻿
namespace ProyectBD_GM.Reportes
{
    partial class ReportesAño
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.AlumnosMatriculadosAsignaturaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataRepAño = new ProyectBD_GM.Reportes.DataRepAño();
            this.AlumnosMatriculadosAñoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.buscar = new System.Windows.Forms.Button();
            this.txtaño = new System.Windows.Forms.TextBox();
            this.AlumnosMatriculadosAñoTableAdapter = new ProyectBD_GM.Reportes.DataRepAñoTableAdapters.AlumnosMatriculadosAñoTableAdapter();
            this.AlumnosMatriculadosAsignaturaTableAdapter = new ProyectBD_GM.Reportes.DataRepAñoTableAdapters.AlumnosMatriculadosAsignaturaTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAsignaturaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataRepAño)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAñoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // AlumnosMatriculadosAsignaturaBindingSource
            // 
            this.AlumnosMatriculadosAsignaturaBindingSource.DataMember = "AlumnosMatriculadosAsignatura";
            this.AlumnosMatriculadosAsignaturaBindingSource.DataSource = this.DataRepAño;
            // 
            // DataRepAño
            // 
            this.DataRepAño.DataSetName = "DataRepAño";
            this.DataRepAño.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AlumnosMatriculadosAñoBindingSource
            // 
            this.AlumnosMatriculadosAñoBindingSource.DataMember = "AlumnosMatriculadosAño";
            this.AlumnosMatriculadosAñoBindingSource.DataSource = this.DataRepAño;
            // 
            // reportViewer1
            // 
            reportDataSource4.Name = "DataSet1";
            reportDataSource4.Value = this.AlumnosMatriculadosAñoBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ProyectBD_GM.Reportes.ReportAño.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 58);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 392);
            this.reportViewer1.TabIndex = 0;
            // 
            // buscar
            // 
            this.buscar.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buscar.Location = new System.Drawing.Point(456, 16);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(110, 23);
            this.buscar.TabIndex = 1;
            this.buscar.Text = "BUSCAR";
            this.buscar.UseVisualStyleBackColor = false;
            this.buscar.Click += new System.EventHandler(this.buscar_Click);
            // 
            // txtaño
            // 
            this.txtaño.Location = new System.Drawing.Point(297, 16);
            this.txtaño.Name = "txtaño";
            this.txtaño.Size = new System.Drawing.Size(137, 20);
            this.txtaño.TabIndex = 2;
            // 
            // AlumnosMatriculadosAñoTableAdapter
            // 
            this.AlumnosMatriculadosAñoTableAdapter.ClearBeforeFill = true;
            // 
            // AlumnosMatriculadosAsignaturaTableAdapter
            // 
            this.AlumnosMatriculadosAsignaturaTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(244, 16);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "AÑO";
            // 
            // ReportesAño
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtaño);
            this.Controls.Add(this.buscar);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ReportesAño";
            this.Text = "ReportesAño";
            this.Load += new System.EventHandler(this.ReportesAño_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAsignaturaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataRepAño)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlumnosMatriculadosAñoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource AlumnosMatriculadosAñoBindingSource;
        private DataRepAño DataRepAño;
        private DataRepAñoTableAdapters.AlumnosMatriculadosAñoTableAdapter AlumnosMatriculadosAñoTableAdapter;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.TextBox txtaño;
        private System.Windows.Forms.BindingSource AlumnosMatriculadosAsignaturaBindingSource;
        private DataRepAñoTableAdapters.AlumnosMatriculadosAsignaturaTableAdapter AlumnosMatriculadosAsignaturaTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}