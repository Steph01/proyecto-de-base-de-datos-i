﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ProyectBD_GM.controller;

namespace ProyectBD_GM.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para CatProfesorUserControl.xaml
    /// </summary>
    public partial class CatProfesorUserControl : UserControl
    {
        private bool IsNuevo = false;
        private bool IsEditar = false;

        public CatProfesorUserControl()
        {
            InitializeComponent();
            Botones();

            pnlBuscar.btnBuscar.Click += new RoutedEventHandler(BtnBuscar_Click);
            pnlBuscar.btnCancelar.Click += new RoutedEventHandler(BtnCancelar_Click);

        }

        public void mostrarProfesor()
        {
            dtGProfesor.DataContext = CProfesor.MostrarProfesor();
        }

        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar) //Alt + 124
            {
                this.Habilitar(true);
                this.btnNuevo.IsEnabled = false;
                this.btnGuardar.IsEnabled = true;
                this.btnModificar.IsEnabled = false;
                this.btnCancelar.IsEnabled = true;
                this.btnMostrar.IsEnabled = false;
            }
            else
            {
                this.Habilitar(false);
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = false;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = false;
                this.btnMostrar.IsEnabled = true;
            }
        }
        private void Habilitar(bool valor)
        {
            this.txtPrimerNombre.IsReadOnly = !valor;
            this.txtSegundoNombre.IsReadOnly = !valor;
            this.txtPrimerApellido.IsReadOnly = !valor;
            this.txtSegundoApellido.IsReadOnly = !valor;
            this.txtDireccion.IsReadOnly = !valor;
            this.txtTelefono.IsReadOnly = !valor;
        }

        private void Limpiar()
        {
            this.txtPrimerNombre.Text = string.Empty;
            this.txtSegundoNombre.Text = string.Empty;
            this.txtPrimerApellido.Text = string.Empty;
            this.txtSegundoApellido.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtTelefono.Text = string.Empty;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.txtPrimerNombre.Focus();
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rpta = "";

                if (this.IsNuevo)
                {
                    rpta = CProfesor.InsertarProfesor(txtPrimerNombre.Text, txtSegundoNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, txtDireccion.Text, int.Parse(txtTelefono.Text));

                }
                else
                {
                    DataRowView row_selected = dtGProfesor.SelectedItem as DataRowView;
                    rpta = CProfesor.EditarProfesor(Convert.ToInt32(row_selected["Id Profesor"].ToString()),
                        this.txtPrimerNombre.Text, this.txtSegundoNombre.Text, this.txtPrimerApellido.Text,
                        this.txtSegundoApellido.Text, this.txtDireccion.Text, Convert.ToInt32(this.txtTelefono.Text));
                    mostrarProfesor();
                }

                if (rpta.Equals("OK"))
                {
                    if (this.IsNuevo)
                    {

                        MessageBox.Show("Datos Ingresados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos Actualizados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {

                    MessageBox.Show(rpta, "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                this.IsNuevo = false;
                this.IsEditar = false;
                this.Botones();
                this.Limpiar();
                mostrarProfesor();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }

        }

        private void btnMostrar_Click(object sender, RoutedEventArgs e)
        {
            mostrarProfesor();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {

            this.IsNuevo = false;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
        }
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGProfesor.SelectedItem as DataRowView;
            if (this.dtGProfesor.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    txtPrimerNombre.Text = row_selected["Primer Nombre"].ToString();
                    txtSegundoNombre.Text = row_selected["Segundo Nombre"].ToString();
                    txtPrimerApellido.Text = row_selected["Primer Apellido"].ToString();
                    txtSegundoApellido.Text = row_selected["Segundo Apellido"].ToString();
                    txtDireccion.Text = row_selected["Dirección"].ToString();
                    txtTelefono.Text = row_selected["Teléfono"].ToString();
                    MessageBox.Show("" + row_selected.GetHashCode());
                    this.IsNuevo = false;
                    this.IsEditar = true;
                    this.Botones();
                    this.txtPrimerNombre.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila antes de Modificar", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);

            }           
        }
        private void btnEstado_Click(object sender, RoutedEventArgs e)
        {
            if (this.dtGProfesor.SelectedItems.Count == 1)
            {
                try
                {
                    DataRowView row_selected = dtGProfesor.SelectedItem as DataRowView;
                    CEstudiante.CambioEstado(Convert.ToInt32(row_selected["IdProfesor"].ToString()));
                    MessageBox.Show("El estado ha sido actualizado", "Gestión Matrícula");

                    this.IsNuevo = false;
                    this.IsEditar = false;
                    this.Botones();
                    this.Limpiar();
                    this.dtGProfesor.DataContext = CProfesor.MostrarProfesor();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace);
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para cambiar el estado", "Sistema de Reservas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            if (pnlBuscar.txtBuscar.Text != string.Empty)
            {
                dtGProfesor.DataContext = CProfesor.BuscarProfesor(pnlBuscar.txtBuscar.Text);
            }
            else
            {
                MessageBox.Show("No hay nada para Buscar xd");
            }
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (pnlBuscar.txtBuscar.Text != string.Empty)
            {
                dtGProfesor.DataContext = CProfesor.MostrarProfesor();
                pnlBuscar.txtBuscar.Text = string.Empty;
            }
        }

        private void Setter_ColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }
        private void pnlBuscar_Loaded(object sender, RoutedEventArgs e)
        {
            this.dtGProfesor.DataContext = CProfesor.MostrarProfesor();
        }

        private void dtGProfesor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}

