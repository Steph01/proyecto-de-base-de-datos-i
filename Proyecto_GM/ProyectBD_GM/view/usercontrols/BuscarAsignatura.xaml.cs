﻿using ProyectBD_GM.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectBD_GM.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para BuscarAsignatura.xaml
    /// </summary>
    public partial class BuscarAsignatura : Window
    {
        public static string Nombre;
        public static string Semestre;
        private InscripcionUserControl ic;

        public BuscarAsignatura(InscripcionUserControl ic)
        {
            InitializeComponent();
            mostrarAsignatura();
            this.ic = ic;
        }
        public void mostrarAsignatura()
        {
            dtGAsignaturas.DataContext = CAsignatura.MostrarAsignatura();
        }
        private void cmbSemestre_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dtGAsignaturas.DataContext = CDetalleMatrícula.MostrarAsignaturas(cmbSemestre.SelectedIndex + 1);
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGAsignaturas.SelectedItem as DataRowView;
            if (this.dtGAsignaturas.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    Nombre = row_selected["Nombre"].ToString();
                    ic.txtAsignatura.Text = Nombre;

                    Semestre = row_selected["Semestre"].ToString();
                    ic.txtSemestre.Text = Semestre;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
