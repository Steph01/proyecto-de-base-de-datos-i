﻿using ProyectBD_GM.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProyectBD_GM.view.usercontrols
{
    /// <summary>
    /// Lógica de interacción para InscripcionUserControl.xaml
    /// </summary>
    public partial class InscripcionUserControl : UserControl
    {
        private bool IsNuevo = false;
        private bool IsEditar = false;
        public InscripcionUserControl()
        {
            InitializeComponent();
            Botones();
            mostrarDetalleMatrícula();
        }
        public void mostrarDetalleMatrícula()
        {
            dtGInscripciones.DataContext = CDetalleMatrícula.MostrarDMatrícula();
        }
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar) //Alt + 124
            {
                this.btnNuevo.IsEnabled = false;
                this.btnGuardar.IsEnabled = true;
                this.btnModificar.IsEnabled = false;
                this.btnCancelar.IsEnabled = true;
                btnseleccionarMatricula.IsEnabled = true;
                txtAula.IsReadOnly = false;
                txtGrupo.IsReadOnly = false;
            }
            else
            {
                this.btnNuevo.IsEnabled = true;
                this.btnGuardar.IsEnabled = false;
                this.btnModificar.IsEnabled = true;
                this.btnCancelar.IsEnabled = false;
                btnseleccionarMatricula.IsEnabled = false;
                txtAula.IsReadOnly = true;
                txtGrupo.IsReadOnly = true;
            }
        }
        private void Limpiar()
        {
            txtNombreEstudiante.Text = string.Empty;
            txtID.Text = string.Empty;
            txtCarrera.Text = string.Empty;
            txtBuscarMatricula.Text = string.Empty;
            txtAula.Text = string.Empty;
            txtGrupo.Text = string.Empty;
        }

        public void mostrarMatricula()
        {
            dtGMatricula.DataContext = CMatricula.MostrarMatricula();
        }

        private void SeleccionarMatricula_Clck(object sender, RoutedEventArgs e)
        {
            DataRowView row_selected = dtGMatricula.SelectedItem as DataRowView;
            if (this.dtGMatricula.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    txtBNumMatricula.Text = row_selected["Número de Matrícula"].ToString();
                    txtNombreEstudiante.Text = row_selected["Nombre"].ToString();
                    txtID.Text = row_selected["ID"].ToString();
                    txtCarrera.Text = row_selected["Nombre Carrera"].ToString();
                    mostrarMatricula();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una Fila antes de Modificar", "Sistema de Gestión de matriculas", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
        }
        private void txtBuscarMatricula_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscarMatricula.Text != string.Empty)
            {
                dtGMatricula.DataContext = CMatricula.BuscarMatricula(txtBuscarMatricula.Text);
            } else
            {
                dtGMatricula.DataContext = CMatricula.MostrarMatricula();
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            mostrarMatricula();
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            btnGuardar.Content = "Inscribir";
            try
            {
                string rpta = "";

                if (this.IsNuevo)
                {
                    rpta = CDetalleMatrícula.Insertar_DetalleMatricula(this.txtNombreEstudiante.Text, txtAsignatura.Text,
                        Convert.ToInt32(txtSemestre.Text), Convert.ToInt32(txtAula.Text), txtGrupo.Text);
                }
                else
                {
                    DataRowView row_selected = dtGInscripciones.SelectedItem as DataRowView;
                    rpta = CDetalleMatrícula.EditarDetalleMatrícula(Convert.ToInt32(row_selected["IdDetalleMatrícula"].ToString()),
                        this.txtNombreEstudiante.Text, this.txtAsignatura.Text, Convert.ToInt32(this.txtSemestre.Text), Convert.ToInt32(this.txtAula.Text), this.txtGrupo.Text);
                }
                if (rpta.Equals("OK"))
                {
                    if (this.IsNuevo)
                    {

                        MessageBox.Show("Datos Ingresados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos Actualizados", "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show(rpta, "Gestión de Matrícula", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                this.IsNuevo = false;
                this.IsEditar = false;
                this.Botones();
                this.Limpiar();
                mostrarDetalleMatrícula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.IsNuevo = false;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
        }
        private void btnBuscarAsignatura_Click(object sender, RoutedEventArgs e)
        {
            BuscarAsignatura ba = new BuscarAsignatura(this);
            ba.ShowDialog();
        }
        private void btnPrueba_Click(object sender, RoutedEventArgs e)
        {
            mostrarDetalleMatrícula();
        }
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            btnGuardar.Content = "Guardar";
            DataRowView row_selected = dtGInscripciones.SelectedItem as DataRowView;
            if (this.dtGInscripciones.SelectedItems.Count == 1)
            {
                if (row_selected != null)
                {
                    txtNombreEstudiante.Text = row_selected["Nombre Estudiante"].ToString();
                    txtAula.Text = row_selected["Aula"].ToString();
                    txtAsignatura.Text = row_selected["Nombre"].ToString();
                    txtGrupo.Text= row_selected["Grupo"].ToString();
                    txtSemestre.Text = row_selected["Semestre"].ToString();

                    IsNuevo = false;
                    IsEditar = true;
                    Botones();
                }
                mostrarMatricula();
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna matrícula.",
                    "Sistema de Gestión de matrículas", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

    }
}
