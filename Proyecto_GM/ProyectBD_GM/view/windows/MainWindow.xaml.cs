﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProyectBD_GM.controller;

namespace ProyectBD_GM.view.windows
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainController mainC;
        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }
        public void SetupController()
        {
            mainC = new MainController(this);
            this.Loaded += new RoutedEventHandler(mainC.MainWindowEventHandler);
            RoutedEventHandler routed = new RoutedEventHandler(mainC.MainMenuEventHandler);
            menuICatalogoEstudiante.Click += routed;
            //menuICatalogoCarrera.Click += routed;
            menuICatalogoAsignatura.Click += routed;
            menuICatalogoProfesor.Click += routed;

            menuIOpMatricula.Click += routed;
            menuIReporteAño.Click += routed;
            menuIReporteAsignatura.Click += routed;
            menuIReporteGrupo.Click += routed;
            menuIReporteDetalleMatricula.Click += routed;
        }
    }
}
