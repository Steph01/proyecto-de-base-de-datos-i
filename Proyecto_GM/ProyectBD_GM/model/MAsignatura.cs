﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ProyectBD_GM.model
{
    class MAsignatura
    {
        private int noAsignatura;
        private String NombreA;
        private int Creditos;
        private String Semestre;
        private char Clasificacion;

        public int No_Asignatura { get => noAsignatura; set => noAsignatura = value;}
        public string Nombre { get => NombreA; set => NombreA = value;}
        public int creditos { get => Creditos; set => Creditos = value;}
        public string semestre { get => Semestre; set => Semestre = value; }
        public char clasificacion { get => Clasificacion; set => Clasificacion = value;}

        public static DataTable MostrarAsignatura()
        {
            DataTable DtResultado = new DataTable("MostrarAsignatura");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando el conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Mostrar_Asignatura";
                SqlCmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }
        public static DataTable BuscarAsignatura(string dato)
        {
            DataTable DtResultado = new DataTable("BuscarAsignatura");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando el conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Buscar_Asignatura";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParDato = new SqlParameter();
                ParDato.ParameterName = "@dato";
                ParDato.SqlDbType = SqlDbType.VarChar;
                ParDato.Size = 60;
                ParDato.Value = dato;
                SqlCmd.Parameters.Add(ParDato);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }
        public string InsertarAsignatura(string NombreA, int Creditos, string Semestre, char Clasificacion)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Insertar_Asignatura";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado

                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@Nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 60;
                ParNombre.Value = NombreA;
                SqlCmd.Parameters.Add(ParNombre);

                SqlParameter ParCreditos = new SqlParameter();
                ParCreditos.ParameterName = "@NoCreditos";
                ParCreditos.SqlDbType = SqlDbType.Int;
                ParCreditos.Value = Creditos;
                SqlCmd.Parameters.Add(ParCreditos);

                SqlParameter ParSemestre = new SqlParameter();
                ParSemestre.ParameterName = "@Semestre";
                ParSemestre.SqlDbType = SqlDbType.VarChar;
                ParSemestre.Size = 60;
                ParSemestre.Value = Semestre;
                SqlCmd.Parameters.Add(ParSemestre);

                SqlParameter ParClasificacion = new SqlParameter();
                ParClasificacion.ParameterName = "@Clasificacion";
                ParClasificacion.SqlDbType = SqlDbType.Char;
                ParClasificacion.Value = Clasificacion;
                SqlCmd.Parameters.Add(ParClasificacion);

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message + "a";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }
        public string EditarAsignatura(int noAsignatura, string Nombre, int Creditos, string Semestre, char Clasificacion)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Editar_Asignatura";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado

                SqlParameter ParNoAsignatura = new SqlParameter();
                ParNoAsignatura.ParameterName = "@NoAsignatura";
                ParNoAsignatura.SqlDbType = SqlDbType.Int;
                ParNoAsignatura.Value = noAsignatura;
                SqlCmd.Parameters.Add(ParNoAsignatura);

                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@Nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 60;
                ParNombre.Value = Nombre;
                SqlCmd.Parameters.Add(ParNombre);

                SqlParameter ParCreditos = new SqlParameter();
                ParCreditos.ParameterName = "@NoCreditos";
                ParCreditos.SqlDbType = SqlDbType.Int;
                ParCreditos.Value = Creditos;
                SqlCmd.Parameters.Add(ParCreditos);

                SqlParameter ParSemestre = new SqlParameter();
                ParSemestre.ParameterName = "@Semestre";
                ParSemestre.SqlDbType = SqlDbType.VarChar;
                ParSemestre.Size = 60;
                ParSemestre.Value = Semestre;
                SqlCmd.Parameters.Add(ParSemestre);

                SqlParameter ParClasificacion = new SqlParameter();
                ParClasificacion.ParameterName = "@Clasificacion";
                ParClasificacion.SqlDbType = SqlDbType.Char;
                ParClasificacion.Value = Clasificacion;
                SqlCmd.Parameters.Add(ParClasificacion);

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }
    }
}
