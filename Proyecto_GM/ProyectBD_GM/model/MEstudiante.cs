﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ProyectBD_GM.model
{
    class MEstudiante
    {
        // Variables de Clase
        private int idEstudiante;
        private String NombreCarrera;
        private String primerNombre;
        private String segundoNombre;
        private String primerApellido;
        private String segundoApellido;
        private DateTime Fecha_Ingreso;
        private String Direccion;
        private int Telefono;
        private String Correo;
        private int Edad;
        public int Id_Estudiante { get => idEstudiante; set => idEstudiante = value; }
        public string N_Carrera { get => NombreCarrera; set => NombreCarrera = value; }
        public string P_nombre { get => primerNombre; set => primerNombre = value; }
        public string S_nombre { get => segundoNombre; set => segundoNombre = value; }
        public string P_apellido { get => primerApellido; set => primerApellido = value; }
        public string S_apellido { get => segundoApellido; set => segundoApellido = value; } 
        public DateTime FechaIngreso { get => Fecha_Ingreso; set => Fecha_Ingreso = value; }
        public string direccion { get => Direccion; set => Direccion = value; }
        public int telefono { get => Telefono; set => Telefono = value; }
        public string correo { get => Correo; set => Correo = value; }
        public int edad { get => Edad; set => Edad = value; }

        public static DataTable CambioEstado(int idEstudiante)
        {
            DataTable DtResultado = new DataTable("CambioEstado");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando la conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Cambio_Estado_Estudiante";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //   Cargando el parámetro de Búsqueda
                SqlParameter ParDato = new SqlParameter();
                ParDato.ParameterName = "@IdEstudiante";
                ParDato.SqlDbType = SqlDbType.Int;
                ParDato.Value = idEstudiante;
                SqlCmd.Parameters.Add(ParDato);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }
        public static DataTable MostrarEstudiante()
        {
            DataTable DtResultado = new DataTable("MostrarEstudiante");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    
                SqlCon.ConnectionString = Conexion.Cn;

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Mostrar_Estudiante";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }
        public static DataTable BuscarEstudiante(string dato)
        {
            DataTable DtResultado = new DataTable("BuscarEstudiante");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando el conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Buscar_Estudiante";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParDato = new SqlParameter();
                ParDato.ParameterName = "@dato";
                ParDato.SqlDbType = SqlDbType.VarChar;
                ParDato.Size = 60;
                ParDato.Value = dato;
                SqlCmd.Parameters.Add(ParDato);


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }
       
        public string InsertarEstudiante(string NombreCarrera, string primerNombre, string segundoNombre, string primerApellido, string segundoApellido,
            DateTime FechaIngreso, string Dirección, int Telefono, string Correo, int Edad)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Insertar_Estudiante";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado

                SqlParameter ParNombreCarrera = new SqlParameter();
                ParNombreCarrera.ParameterName = "@NombreCarrera";
                ParNombreCarrera.SqlDbType = SqlDbType.VarChar;
                ParNombreCarrera.Size = 60;
                ParNombreCarrera.Value = NombreCarrera;
                SqlCmd.Parameters.Add(ParNombreCarrera);

                SqlParameter ParPrimerNombre = new SqlParameter();
                ParPrimerNombre.ParameterName = "@PrimerNombre";
                ParPrimerNombre.SqlDbType = SqlDbType.VarChar;
                ParPrimerNombre.Size = 60;
                ParPrimerNombre.Value = primerNombre;
                SqlCmd.Parameters.Add(ParPrimerNombre);

                SqlParameter ParSegundoNombre = new SqlParameter();
                ParSegundoNombre.ParameterName = "@SegundoNombre";
                ParSegundoNombre.SqlDbType = SqlDbType.VarChar;
                ParSegundoNombre.Size = 60;
                ParSegundoNombre.Value = segundoNombre;
                SqlCmd.Parameters.Add(ParSegundoNombre);

                SqlParameter ParPrimerApellido = new SqlParameter();
                ParPrimerApellido.ParameterName = "@PrimerApellido";
                ParPrimerApellido.SqlDbType = SqlDbType.VarChar;
                ParPrimerApellido.Size = 60;
                ParPrimerApellido.Value = primerApellido;
                SqlCmd.Parameters.Add(ParPrimerApellido);

                SqlParameter ParSegundoApellido = new SqlParameter();
                ParSegundoApellido.ParameterName = "@SegundoApellido";
                ParSegundoApellido.SqlDbType = SqlDbType.VarChar;
                ParSegundoApellido.Size = 60;
                ParSegundoApellido.Value = segundoApellido;
                SqlCmd.Parameters.Add(ParSegundoApellido);

                SqlParameter parFechaIngreso = new SqlParameter();
                parFechaIngreso.ParameterName = "@Fecha_Ingreso";
                parFechaIngreso.SqlDbType = SqlDbType.DateTime;
                parFechaIngreso.Value = FechaIngreso;
                SqlCmd.Parameters.Add(parFechaIngreso);

                SqlParameter ParDirección = new SqlParameter();
                ParDirección.ParameterName = "@Direccion";
                ParDirección.SqlDbType = SqlDbType.VarChar;
                ParDirección.Size = 100;
                ParDirección.Value = Dirección;
                SqlCmd.Parameters.Add(ParDirección);

                SqlParameter ParTelefono = new SqlParameter();
                ParTelefono.ParameterName = "@Teléfono";
                ParTelefono.SqlDbType = SqlDbType.Int;
                ParTelefono.Value = Telefono;
                SqlCmd.Parameters.Add(ParTelefono);

                SqlParameter ParCorreo = new SqlParameter();
                ParCorreo.ParameterName = "@Correo";
                ParCorreo.SqlDbType = SqlDbType.VarChar;
                ParCorreo.Size = 60;
                ParCorreo.Value = Correo;
                SqlCmd.Parameters.Add(ParCorreo);

                SqlParameter ParEdad = new SqlParameter();
                ParEdad.ParameterName = "@Edad";
                ParEdad.SqlDbType = SqlDbType.Int;
                ParEdad.Value = Edad;
                SqlCmd.Parameters.Add(ParEdad);

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message + "añlsdkfalsdkm";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }
        public string EditarEstudiante(int idEstudiante, string NombreCarrera,string primerNombre, string segundoNombre, string primerApellido, 
            string segundoApellido, DateTime FechaIngreso, string correo, int telefono, string Dirección, int Edad)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Editar_Estudiante";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado
                SqlParameter ParIdEstudiante = new SqlParameter();
                ParIdEstudiante.ParameterName = "@IdEstudiante";
                ParIdEstudiante.SqlDbType = SqlDbType.Int;
                ParIdEstudiante.Value = idEstudiante;
                SqlCmd.Parameters.Add(ParIdEstudiante);

                SqlParameter ParCarrera = new SqlParameter();
                ParCarrera.ParameterName = "@NombreCarrera";
                ParCarrera.SqlDbType = SqlDbType.VarChar;
                ParCarrera.Size = 50;
                ParCarrera.Value = NombreCarrera;
                SqlCmd.Parameters.Add(ParCarrera);

                SqlParameter ParPrimerNombre = new SqlParameter();
                ParPrimerNombre.ParameterName = "@PrimerNombre";
                ParPrimerNombre.SqlDbType = SqlDbType.VarChar;
                ParPrimerNombre.Size = 60;
                ParPrimerNombre.Value = primerNombre;
                SqlCmd.Parameters.Add(ParPrimerNombre);

                SqlParameter ParSegundoNombre = new SqlParameter();
                ParSegundoNombre.ParameterName = "@SegundoNombre";
                ParSegundoNombre.SqlDbType = SqlDbType.VarChar;
                ParSegundoNombre.Size = 60;
                ParSegundoNombre.Value = segundoNombre;
                SqlCmd.Parameters.Add(ParSegundoNombre);

                SqlParameter ParPrimerApellido = new SqlParameter();
                ParPrimerApellido.ParameterName = "@PrimerApellido";
                ParPrimerApellido.SqlDbType = SqlDbType.VarChar;
                ParPrimerApellido.Size = 60;
                ParPrimerApellido.Value = primerApellido;
                SqlCmd.Parameters.Add(ParPrimerApellido);

                SqlParameter ParSegundoApellido = new SqlParameter();
                ParSegundoApellido.ParameterName = "@SegundoApellido";
                ParSegundoApellido.SqlDbType = SqlDbType.VarChar;
                ParSegundoApellido.Size = 60;
                ParSegundoApellido.Value = segundoApellido;
                SqlCmd.Parameters.Add(ParSegundoApellido);

                SqlParameter parFechaIngreso = new SqlParameter();
                parFechaIngreso.ParameterName = "@Fecha_Ingreso";
                parFechaIngreso.SqlDbType = SqlDbType.DateTime;
                parFechaIngreso.Value = FechaIngreso;
                SqlCmd.Parameters.Add(parFechaIngreso);

                SqlParameter ParDirección = new SqlParameter();
                ParDirección.ParameterName = "@Direccion";
                ParDirección.SqlDbType = SqlDbType.VarChar;
                ParDirección.Size = 100;
                ParDirección.Value = Dirección;
                SqlCmd.Parameters.Add(ParDirección);

                SqlParameter ParTelefono = new SqlParameter();
                ParTelefono.ParameterName = "@Telefono";
                ParTelefono.SqlDbType = SqlDbType.Int;
                ParTelefono.Value = telefono;
                SqlCmd.Parameters.Add(ParTelefono);

                SqlParameter ParCorreo = new SqlParameter();
                ParCorreo.ParameterName = "@Correo";
                ParCorreo.SqlDbType = SqlDbType.VarChar;
                ParCorreo.Size = 60;
                ParCorreo.Value = correo;
                SqlCmd.Parameters.Add(ParCorreo);

                SqlParameter ParEdad = new SqlParameter();
                ParEdad.ParameterName = "@Edad";
                ParEdad.SqlDbType = SqlDbType.Int;
                ParEdad.Value = Edad;
                SqlCmd.Parameters.Add(ParEdad);

                //Ejecutamos nuestro comando
                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }
    }
}
