﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace ProyectBD_GM.model
{
    class MMatricula
    {

        private int NoMatricula;
        private String NombreEstudiante;
        private int Año;

        public int No_Matricula { get => NoMatricula; set => NoMatricula = value; }
        public string N_Estudiante { get => NombreEstudiante; set => NombreEstudiante = value; }
        public int año { get => Año; set => Año = value; }

        public static DataTable MostrarMatricula()
        {
            DataTable DtResultado = new DataTable("MostrarMatricula");
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando el conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Mostrar_Matricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
            {
                DtResultado = null;
            }
            return DtResultado;
        }

        public string InsertarMatricula(string NombreEstudiante,int Año)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Insertar_Matrícula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado

                SqlParameter ParNombreEstudiante = new SqlParameter();
                ParNombreEstudiante.ParameterName = "@NombreEstudiante";
                ParNombreEstudiante.SqlDbType = SqlDbType.VarChar;
                ParNombreEstudiante.Size = 60;
                ParNombreEstudiante.Value = NombreEstudiante;
                SqlCmd.Parameters.Add(ParNombreEstudiante);

                SqlParameter ParAño = new SqlParameter();
                ParAño.ParameterName = "@Año";
                ParAño.SqlDbType = SqlDbType.Int;
                ParAño.Value = Año;
                SqlCmd.Parameters.Add(ParAño);
                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message + "añlsdkfalsdkm";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }
        public string EditarMatricula(int NoMatricula, string NombreEstudiante, int Año)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Editar_Matricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParMatricula = new SqlParameter();
                ParMatricula.ParameterName = "@NMatricula";
                ParMatricula.SqlDbType = SqlDbType.Int;
                ParMatricula.Value = NoMatricula;
                SqlCmd.Parameters.Add(ParMatricula);

                SqlParameter ParEstudiante = new SqlParameter();
                ParEstudiante.ParameterName = "@NombreEstudiante";
                ParEstudiante.SqlDbType = SqlDbType.VarChar;
                ParEstudiante.Size = 50;
                ParEstudiante.Value = NombreEstudiante;
                SqlCmd.Parameters.Add(ParEstudiante);

                SqlParameter ParAño = new SqlParameter();
                ParAño.ParameterName = "@Año";
                ParAño.SqlDbType = SqlDbType.Int;
                ParAño.Value = Año;
                SqlCmd.Parameters.Add(ParAño);
                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }

        public static DataTable BuscarMatricula(int idMatricula)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();
            try
            {    // Cargando el conexión al servidor
                SqlCon.ConnectionString = Conexion.Cn;
                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Buscar_Matricula";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParDato = new SqlParameter();
                ParDato.ParameterName = "@dato";
                ParDato.SqlDbType = SqlDbType.Int;
                ParDato.Value = idMatricula;
                SqlCmd.Parameters.Add(ParDato);


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }

            catch (Exception ex)
            {
                MessageBox.Show("A ocurrido un error al intentar buscar la matrícula.");
                DtResultado = null;
            }
            return DtResultado;
        }
    }
}
